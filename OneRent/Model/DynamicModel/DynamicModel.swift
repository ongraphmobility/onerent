/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct DynamicModel : Codable {
	let id : Int?
	let zipcode_required_field : Bool?
	let pricing : String?
	let agreement_age_text : String?
	let bad_weather : Bool?
	let agreement_file : String?
	let extra_min_text : String?
    let extra_min : Int?
	let main_logo_screen_tagline : String?
	let screen_after_lock : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case zipcode_required_field = "zipcode_required_field"
		case pricing = "pricing"
		case agreement_age_text = "agreement_age_text"
		case bad_weather = "bad_weather"
		case agreement_file = "agreement_file"
		case extra_min_text = "extra_min_text"
        case extra_min = "extra_min"
		case main_logo_screen_tagline = "main_logo_screen_tagline"
		case screen_after_lock = "screen_after_lock"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		zipcode_required_field = try values.decodeIfPresent(Bool.self, forKey: .zipcode_required_field)
		pricing = try values.decodeIfPresent(String.self, forKey: .pricing)
		agreement_age_text = try values.decodeIfPresent(String.self, forKey: .agreement_age_text)
		bad_weather = try values.decodeIfPresent(Bool.self, forKey: .bad_weather)
		agreement_file = try values.decodeIfPresent(String.self, forKey: .agreement_file)
		extra_min_text = try values.decodeIfPresent(String.self, forKey: .extra_min_text)
        extra_min = try values.decodeIfPresent(Int.self, forKey: .extra_min)
		main_logo_screen_tagline = try values.decodeIfPresent(String.self, forKey: .main_logo_screen_tagline)
		screen_after_lock = try values.decodeIfPresent(String.self, forKey: .screen_after_lock)
	}

}
