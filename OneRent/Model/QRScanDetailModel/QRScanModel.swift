

import Foundation
struct QRScanModel : Codable {
	let board_Name : String?
	let before3hour : Double?
	let image : String?
    let start_booking_time : String?
    let board_submission_time : String?
    let end_booking_time : String?
	let status : String?
    let message : String?
    let active_board : Bool?
	let after3hour : Double?

	enum CodingKeys: String, CodingKey {

		case board_Name = "board_Name"
		case before3hour = "Before3hour"
		case image = "image"
		case status = "status"
        case active_board = "active_board"
        case message = "message"
		case after3hour = "After3hour"
        case start_booking_time = "start_booking_time"
        case board_submission_time = "board_submission_time"
        case end_booking_time = "end_booking_time"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		board_Name = try values.decodeIfPresent(String.self, forKey: .board_Name)
        before3hour = try values.decodeIfPresent(Double.self, forKey: .before3hour)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		status = try values.decodeIfPresent(String.self, forKey: .status)
        active_board = try values.decodeIfPresent(Bool.self, forKey: .active_board)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        after3hour = try values.decodeIfPresent(Double.self, forKey: .after3hour)
        start_booking_time = try values.decodeIfPresent(String.self, forKey: .start_booking_time)
        board_submission_time = try values.decodeIfPresent(String.self, forKey: .board_submission_time)
        end_booking_time = try values.decodeIfPresent(String.self, forKey: .end_booking_time)
	}

}
