/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct BranchModel : Codable {
	let clicked_branch_link : Bool?
	let is_first_session : Bool?
	let click_timestamp : Int?
	let referring_link : String?
	let user : BranchUser?
	let id : String?
	let ios_passive_deepview : String?
	let one_time_use : Bool?
	let creation_source : Int?
	let match_guaranteed : Bool?

	enum CodingKeys: String, CodingKey {

		case clicked_branch_link = "+clicked_branch_link"
		case is_first_session = "+is_first_session"
		case click_timestamp = "+click_timestamp"
		case referring_link = "~referring_link"
		case user = "user"
		case id = "~id"
		case ios_passive_deepview = "$ios_passive_deepview"
		case one_time_use = "$one_time_use"
		case creation_source = "~creation_source"
		case match_guaranteed = "+match_guaranteed"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		clicked_branch_link = try values.decodeIfPresent(Bool.self, forKey: .clicked_branch_link)
		is_first_session = try values.decodeIfPresent(Bool.self, forKey: .is_first_session)
		click_timestamp = try values.decodeIfPresent(Int.self, forKey: .click_timestamp)
		referring_link = try values.decodeIfPresent(String.self, forKey: .referring_link)
		user = try values.decodeIfPresent(BranchUser.self, forKey: .user)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		ios_passive_deepview = try values.decodeIfPresent(String.self, forKey: .ios_passive_deepview)
		one_time_use = try values.decodeIfPresent(Bool.self, forKey: .one_time_use)
		creation_source = try values.decodeIfPresent(Int.self, forKey: .creation_source)
		match_guaranteed = try values.decodeIfPresent(Bool.self, forKey: .match_guaranteed)
	}

}
