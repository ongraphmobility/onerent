/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct HistoryData : Codable {
	let id : String?
	let board_Id : Int?
	let start_ride_time : String?
	let end_ride_time : String?
	let ride_Time : String?
	let ride_time_amount : String?
	let extra_time_taken_after_ending_a_ride : String?
	let extra_time_amount : String?
	let feedback : HistoryFeedback?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case board_Id = "Board_Id"
		case start_ride_time = "start_ride_time"
		case end_ride_time = "end_ride_time"
		case ride_Time = "ride_Time"
		case ride_time_amount = "ride_time_amount"
		case extra_time_taken_after_ending_a_ride = "extra_time_taken_after_ending_a_ride"
		case extra_time_amount = "extra_time_amount"
		case feedback = "feedback"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
        board_Id = try values.decodeIfPresent(Int.self, forKey: .board_Id)
		start_ride_time = try values.decodeIfPresent(String.self, forKey: .start_ride_time)
		end_ride_time = try values.decodeIfPresent(String.self, forKey: .end_ride_time)
		ride_Time = try values.decodeIfPresent(String.self, forKey: .ride_Time)
		ride_time_amount = try values.decodeIfPresent(String.self, forKey: .ride_time_amount)
		extra_time_taken_after_ending_a_ride = try values.decodeIfPresent(String.self, forKey: .extra_time_taken_after_ending_a_ride)
		extra_time_amount = try values.decodeIfPresent(String.self, forKey: .extra_time_amount)
		feedback = try values.decodeIfPresent(HistoryFeedback.self, forKey: .feedback)
	}

}
