/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct PlatformData : Codable {
    let board_Name : String?
    let unused_boat : Int?
    let price_before_3_hours : Double?
    let price_after_3_hours : Double?
    let total_boats : Int?
    let image : String?
    let board_details_file : String?
    
    enum CodingKeys: String, CodingKey {
        
        case board_Name = "board_Name"
        case unused_boat = "unused_boat"
        case price_before_3_hours = "price_before_3_hours"
        case price_after_3_hours = "price_after_3_hours"
        case total_boats = "total_boats"
        case image = "image"
        case board_details_file = "board_details_file"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        board_Name = try values.decodeIfPresent(String.self, forKey: .board_Name)
        unused_boat = try values.decodeIfPresent(Int.self, forKey: .unused_boat)
        price_before_3_hours = try values.decodeIfPresent(Double.self, forKey: .price_before_3_hours)
        price_after_3_hours = try values.decodeIfPresent(Double.self, forKey: .price_after_3_hours)
        total_boats = try values.decodeIfPresent(Int.self, forKey: .total_boats)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        board_details_file = try values.decodeIfPresent(String.self, forKey: .board_details_file)
    }


}
