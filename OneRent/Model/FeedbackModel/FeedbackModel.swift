

import Foundation
struct FeedbackModel : Codable {
	let id : Int?
	let start_ride_time : String?
	let end_ride_time : String?
	let ride_Time : String?
	let history_Created_Time : String?
	let extra_time_taken_after_ending_a_ride : String?
	let extra_time_amount : String?
	let ride_time_amount : String?
	let user3 : Int?
	let board_Id : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case start_ride_time = "start_ride_time"
		case end_ride_time = "end_ride_time"
		case ride_Time = "ride_Time"
		case history_Created_Time = "history_Created_Time"
		case extra_time_taken_after_ending_a_ride = "extra_time_taken_after_ending_a_ride"
		case extra_time_amount = "extra_time_amount"
		case ride_time_amount = "ride_time_amount"
		case user3 = "user3"
		case board_Id = "Board_Id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		start_ride_time = try values.decodeIfPresent(String.self, forKey: .start_ride_time)
		end_ride_time = try values.decodeIfPresent(String.self, forKey: .end_ride_time)
		ride_Time = try values.decodeIfPresent(String.self, forKey: .ride_Time)
		history_Created_Time = try values.decodeIfPresent(String.self, forKey: .history_Created_Time)
		extra_time_taken_after_ending_a_ride = try values.decodeIfPresent(String.self, forKey: .extra_time_taken_after_ending_a_ride)
		extra_time_amount = try values.decodeIfPresent(String.self, forKey: .extra_time_amount)
		ride_time_amount = try values.decodeIfPresent(String.self, forKey: .ride_time_amount)
		user3 = try values.decodeIfPresent(Int.self, forKey: .user3)
        board_Id = try values.decodeIfPresent(Int.self, forKey: .board_Id)
	}

}
