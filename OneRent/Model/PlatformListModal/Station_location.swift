

import Foundation
struct Station_location : Codable {
	let id : String?
	let platformName : String?
	let latitude : String?
	let longitude : String?
	let numberOfBoards : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case platformName = "platformName"
		case latitude = "latitude"
		case longitude = "longitude"
		case numberOfBoards = "numberOfBoards"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		platformName = try values.decodeIfPresent(String.self, forKey: .platformName)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		numberOfBoards = try values.decodeIfPresent(String.self, forKey: .numberOfBoards)
	}

}
