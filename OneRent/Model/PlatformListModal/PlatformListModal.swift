


import Foundation
struct PlatformListModal : Codable {
	let message : String?
	let success : String?
	let station_location : [Station_location]?

	enum CodingKeys: String, CodingKey {

		case message = "message"
		case success = "success"
		case station_location = "station_location"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		success = try values.decodeIfPresent(String.self, forKey: .success)
		station_location = try values.decodeIfPresent([Station_location].self, forKey: .station_location)
	}

}
