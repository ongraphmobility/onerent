/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct AppCancelData : Codable {
	let start_ride_time : String?
	let board_Name : String?
	let image : String?
	let before3hour : Double?
	let after3hour : Double?
	let start_booking_time : String?
	let end_booking_time : String?
	let board_submission_time : String?
	let status : Bool?

	enum CodingKeys: String, CodingKey {

		case start_ride_time = "start_ride_time"
		case board_Name = "board_Name"
		case image = "image"
		case before3hour = "Before3hour"
		case after3hour = "After3hour"
		case start_booking_time = "start_booking_time"
		case end_booking_time = "end_booking_time"
		case board_submission_time = "board_submission_time"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		start_ride_time = try values.decodeIfPresent(String.self, forKey: .start_ride_time)
		board_Name = try values.decodeIfPresent(String.self, forKey: .board_Name)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		before3hour = try values.decodeIfPresent(Double.self, forKey: .before3hour)
		after3hour = try values.decodeIfPresent(Double.self, forKey: .after3hour)
		start_booking_time = try values.decodeIfPresent(String.self, forKey: .start_booking_time)
		end_booking_time = try values.decodeIfPresent(String.self, forKey: .end_booking_time)
		board_submission_time = try values.decodeIfPresent(String.self, forKey: .board_submission_time)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
	}

}
