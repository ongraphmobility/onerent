//
//  NetworkManager.swift
//  OneRent
//
//  Created by Ongraph Technology on 25/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class NetworkManager{
    
    static var topViewController : UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    static let shared = NetworkManager()
    var rechabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    typealias result = (Bool,Data?) -> Void
    class func callApi(baseUrl: String,requestType: RequestType, headers: HTTPHeaders?, parameters: Parameters?, apiName: String, getQuery: String = "", showLoading: Bool , result : @escaping result){
        var httpMethod : HTTPMethod {
            switch requestType {
            case .delete:
                return .delete
            case .get:
                return .get
            case .put:
                return .put
            case .post:
                return .post
            }
        }
        if (NetworkReachabilityManager()?.isReachable)! {
            if showLoading {
              ProgressHUD.show(STRINGS.LOADING)
            }
            Alamofire.request( baseUrl + apiName, method: httpMethod, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate().responseString { (response) in
                print(response.result)
                switch response.result {
                case .success(let value):
                    print(value,"pagerwa")
                    
                    result(true,response.data)
                case .failure(let error):
                    result(false,nil)
                    print(error.localizedDescription)
                }
                if showLoading {
                    ProgressHUD.dismiss()
                
                }
            }
        }else {
            let alert = UIAlertController(title: "Network Not Available", message: "Please Check Your Internet Connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                NetworkManager.callApi(baseUrl: baseUrl, requestType: requestType, headers: headers, parameters: parameters, apiName: apiName, showLoading: false) { (check, data) in
                                     result(check,data)
                                }
                result(false,nil)
            }))
            NetworkManager.topViewController?.present(alert, animated: true, completion: nil)
        }
    }
    // MARK:- ******* Rechability Function *****
    class func isRechableVia(networkStatus: @escaping (NetworkReachabilityManager.NetworkReachabilityStatus) -> Void) {
        NetworkManager.shared.rechabilityManager?.listener = { status in
            switch status {
            case .notReachable:
                networkStatus(.notReachable)
                print("The network is not reachable")
            case .unknown :
                networkStatus(.unknown)
                print("It is unknown whether the network is reachable")
            case .reachable(.ethernetOrWiFi):
                networkStatus(.reachable(.ethernetOrWiFi))
                print("The network is reachable over the WiFi connection")
            case .reachable(.wwan):
                networkStatus(.reachable(.wwan))
                print("The network is reachable over the WWAN connection")
            }
        }
    }
    class func cancelAllRequests() {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
    typealias resultt = (Bool,JSON?) -> Void
    class func uploadFile(baseUrl: String, apiName: String, headers: HTTPHeaders?, requestType: RequestType, parameters: Parameters?, fileKey: String = "profile_picture", fileName: String, showLoading: Bool = true,imageData: Data , resultValue : @escaping resultt, uploadProgress: @escaping (Double) -> Void) {
        if showLoading {
            ProgressHUD.show(STRINGS.LOADING)
        }
        var httpMethod : HTTPMethod {
            switch requestType {
            case .delete:
                return .delete
            case .get:
                return .get
            case .put:
                return .put
            case .post:
                return .post
            }
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(imageData, withName: fileKey, fileName: fileName, mimeType: "image/png")

        }, to: baseUrl + apiName, method: httpMethod, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    uploadProgress(progress.fractionCompleted)
                })
                upload.responseString(completionHandler: { (dataString) in
                    switch dataString.result {
                    case .success(let value):
                        resultValue(true,JSON(parseJSON: value))
                    case .failure(let error):
                        resultValue(false,nil)
                        debugPrint(error.localizedDescription)
                    }
                    if showLoading {
                        ProgressHUD.dismiss()
                    }
                })
            case .failure(let error):
                debugPrint(error.localizedDescription)
                resultValue(false,nil)
            }
        }
    }
  
    
}

enum RequestType : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
