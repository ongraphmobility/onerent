//
//  AppDelegate.swift
//  OneRent
//
//  Created by Ongraph Technology on 24/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManager
import Branch


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appLinkaAPIManager : AppLinkaAPIManager!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        callApiForDynamic()
        IQKeyboardManager.shared().isEnabled = true
//       // let branch: Branch = Branch.getInstance()
//        Branch.getInstance().validateSDKIntegration()
//        //TODO: Remove for launch
//        Branch.useTestBranchKey()
        GMSServices.provideAPIKey("AIzaSyAs_Pl-kes2YnTg1qukpW5iQxjLVBGMwZg")
        GMSPlacesClient.provideAPIKey("AIzaSyAs_Pl-kes2YnTg1qukpW5iQxjLVBGMwZg")
        
//        STPPaymentConfiguration.shared().publishableKey = AppUrlKey.StripeTestKey
        //:- Uncommment below line while moving on production
        STPPaymentConfiguration.shared().publishableKey = AppUrlKey.StripeLiveKey

        //******************** Linka Lock Config *******************//
        appLinkaAPIManager = AppLinkaAPIManager()
        appLinkaAPIManager.initializeLocationManager()
        LinkaAPIService.setAPIProtocol(_protocol: appLinkaAPIManager)
        LocksController.initialize()
        _ = LinkaBLECentralManager.sharedInstance()
        
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler:{ params, error in
            if error == nil {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // ... insert custom logic here ...
                print("params: %@", params as? [String: AnyObject] ?? {})
                print(CommonMethods.getJsonText(dictinary: params! as NSDictionary))
                
                if let isClicked = params?["+clicked_branch_link"] as? Bool {
                    if isClicked{
                        print("came from email link")
                        
                        if let vc = (self.window?.rootViewController as? UINavigationController)?.topViewController as? VerifyViewController{
                            let user = params?["user"] as? NSDictionary
                            print(user)
                            vc.callApiVerifyLoginCode(code: user!["code"] as? Int64 ?? 0)
                            //vc.callApiVerifyLoginCode(code: params?["+clicked_branch_link"])
                            
                        }
                    }
                }
            }
        })

        return true
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool{
        print("url is \(url.absoluteString)")
        // Pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the
            // Facebook SDK, Pinterest SDK, etc
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handledByBranch = Branch.getInstance().continue(userActivity)
        
        return handledByBranch
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification launchOptions: [AnyHashable: Any]) -> Void {
        Branch.getInstance().handlePushNotification(launchOptions)
    }
    
    

   
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "OneRent")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func callApiForDynamic(){
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: header, parameters: nil, apiName: AppApis.dynamic, showLoading: false) { (true, result) in
            if let results = result{
                    UserDefaults.standard.set(results, forKey: STRINGS.USER_DYNAMIC_DATA) 
                    UserDefaults.standard.synchronize()
                    self.moveToSplash()
                }
            }
        }
    
    func moveToSplash(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let splashViewControllerObj = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.SPLASHVC) as! SplashViewController
        let navigatioStack = UINavigationController(rootViewController: splashViewControllerObj)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
        
    }
}





