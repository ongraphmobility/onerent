//
//  AvailableBLELocksViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 21/01/19.
//  Copyright © 2019 Ongraph Technology. All rights reserved.
//

import UIKit
protocol BLEStatusDelegate {
    
    func statusDelegate(_ status: Bool, communicationManager:LinkaBLECommunicationManager!, _ isDeviceLocked: Bool )
    
}
class AvailableBLELocksViewController: UIViewController, LinkaBLECentralManagerDiscoverDelegate,CBCentralManagerDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var loadingView : UIView!
    
    var connected_linkas : [Linka] = [Linka]()
    var list : [DiscoveredPeripherals] = [DiscoveredPeripherals]()
    var linkaList : [Linka] = [Linka]()
    var isShowingLoading = false
    var isDeviceUnLocked : Bool?
    
    var bleStatusDelegate : BLEStatusDelegate!
    
    //For BLE USe
    var manager:CBCentralManager!
    
    
    // For Use to config BLE
    var apiKey : String?
    var secretKey : String?
    var accessToken : String?
    var status : String?
    var timer = Timer()
    
    //For Use After pair-Up Device
    var connectManager : LinkaBLEConnectManager!
    var communicationManager : LinkaBLECommunicationManager!
    var isConnected : Bool = false
    var lockStatusPacket : LockStatusPacket!
    var lockInfoPacket : LockInfoPacket!
    var lockContextPacket : LockContextPacket!
    var RSSI : Int = 0
    var output : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager          = CBCentralManager()
        manager.delegate = self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isShowingLoading = true
        self.loadingView.isHidden = false
        /* Log Level
         * Set the Log Level of the LINKA API Kit
         *
         * You can set the Log to be saved as a String
         * You can also call Helpers.getDebugString() to get the debug log
         * Or Helpers.clearDebugString() to clear the string
         */
        Helpers.setLogLevel(.DEBUG_AND_SAVE_TO_STRING)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if connectManager != nil{
            communicationManager.stopCommunication()
            _ = connectManager.disconnect()
            connectManager.stopCommunication()
        }
    }
    func getApiAndSecretKey(){
        apiKey = "API Key: \(AppLinkaAPIManager.LinkaMerchantAPIKey)"
        secretKey = "Secret Key: \(AppLinkaAPIManager.LinkaMerchantSecretKey)"
        accessToken = "Access Token: \(LinkaMerchantAPIService.getAccessToken()!)"
        status = "Status " + String(LinkaAPIKitVersionNumber)
        print(String(format: "%@,%@,%@,%@", apiKey!,secretKey!,accessToken!,status!))
        fetchAccessToken()
    }
    @IBAction func retryBtnActn(_ sender : UIButton){
        self.loadingView.isHidden = false
        self.stopScan()
        self.startScan()
        initializeTimer()
    }
    //Mark: - @IBActions
    func fetchAccessToken() {
        if self.isDeviceUnLocked == true{
            CommonMethods.getSnackBarWithMessage(message: "Please reconnect to lock the board")
        }
        _ = LinkaMerchantAPIService.fetch_access_token { (responseObject, error) in
            
            DispatchQueue.main.async(execute: {
                
                if responseObject != nil {
                    
                    self.accessToken = "Access Token: \(LinkaMerchantAPIService.getAccessToken())"
                    self.status = "Obtain access token success"
                    self.startScan()
                    self.initializeTimer()
                    return
                    
                } else if error != nil {
                    
                    self.status = "Error: \(error!.statusCode) \(error!.message)"
                    return
                    
                } else {
                    
                    self.status = "Network Error"
                    return
                }
            })
        }
    }
    func initializeTimer(){
       timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.showAlert), userInfo: nil, repeats: true)

    }
    @objc func showAlert(){
        if linkaList.count > 0{
            timer.invalidate()
            return
        }
        self.loadingView.isHidden = true
        CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.NO_BOARD, sender: self)
        timer.invalidate()
    }
    func startScan() {
        LinkaBLECentralManager.sharedInstance().migrateDatabase() // TODO Refactor location
        LinkaBLECentralManager.sharedInstance().addDiscoverDelegate(delegate: self)
        LinkaBLECentralManager.sharedInstance().startScan()
    }
    func stopScan() {
        LinkaBLECentralManager.sharedInstance().removeDiscoverDelegate(delegate: self)
        LinkaBLECentralManager.sharedInstance().stopScan()
        LinkaBLECentralManager.sharedInstance().scannedPeripherals.removeAll()
        linkaList.removeAll()
        list.removeAll()
        tableView.reloadData()
    }
    @IBAction func backBtnActn(_ sender : UIButton){
    self.navigationController?.popViewController(animated: true)
    }
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = linkaList.count
        if count > 0 {
            if self.isShowingLoading {
                self.isShowingLoading = false
            }
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScannedPeriferalDeviceCell") as! ScannedPeriferalDeviceCell
        let linka = linkaList[indexPath.row]
        cell.name.text = linka.getName() + " " + linka.getMACAddress()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row >= linkaList.count {
            return
        }
        ProgressHUD.show("Pairing...")
        let p = list[indexPath.row]
        self.tryPreparePairingUp(p)
//        let ac = UIAlertController(title: nil, message: "Select Action", preferredStyle: UIAlertController.Style.actionSheet)
//
//        ac.popoverPresentationController?.sourceView = self.navigationController?.view
//
//        ac.addAction(UIAlertAction(title: "Declare Ownership Only", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
//            self.tryDeclareOwnershipOnly(p)
//        }))
//        ac.addAction(UIAlertAction(title: "Pair Up -> Connect", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
//            self.tryPreparePairingUp(p)
//        }))
//        ac.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
//
//        }))
//
//        self.present(ac, animated: true, completion: nil)
    }
    
    
//    // DECLARE OWNERSHIP ONLY
//    func tryDeclareOwnershipOnly(_ p : DiscoveredPeripherals) {
//
//        self.loadingView.isHidden = false
//
//        LinkaMerchantAPIService.tryDeclareLockOwnership( p, callback: { (linka, isValid, showError, errorCode) in
//
//            DispatchQueue.main.async(execute: {
//
//                self.loadingView.isHidden = true
//
//                if !isValid && showError {
//
//                    // it's registration fail / network error
//                    UIAlertView(title: "Error", message: "\(errorCode)", delegate: nil, cancelButtonTitle: "OK").show()
//                    self.status = "Error: \(errorCode)"
//
//                    return
//
//                } else {
//
//                    self.status = "Successfully Declared ownership of the lock"
//                }
//            })
//        })
//    }
    
    // PAIR UP
    func tryPreparePairingUp(_ p : DiscoveredPeripherals) {
        
        self.loadingView.isHidden = false
        
        LinkaMerchantAPIService.tryPreparePairingUp( p, progressCallback: { (isLoading, state) in
            
            DispatchQueue.main.async(execute: {
                
                if state == 0 {
                    // registering access keys
                    self.status = "Registering access keys"
                    
                } else if state == 1 {
                    
                    // pairing up lock
                    self.status = "Pairing up lock"
                    ProgressHUD.show("Plesae wait while connecting to the board")
                }
            })
            
        }, callback: { (linka, isValid, showError, errorCode) in
            
            DispatchQueue.main.async(execute: {
                
                self.loadingView.isHidden = true
                
                if !isValid && showError {
                    
                    // it's registration fail / network error
                    UIAlertView(title: "Error", message: "\(errorCode)", delegate: nil, cancelButtonTitle: "OK").show()
                    self.status = "Error: \(errorCode)"
                    return
                } else {
                    self.status = "Successfully paired up the lock"
                }
            })
            
        }, successCallback: { (connectManager, communicationManager) in
            
            DispatchQueue.main.async(execute: {
                
                self.connectManager = connectManager
                self.communicationManager = communicationManager
                self.communactingMangerConfig()
                //                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommunicationViewController") as! CommunicationViewController
                //                vc.connectManager = connectManager
                //                vc.communicationManager = communicationManager
                //                self.navigationController?.pushViewController(vc, animated: true)
//                if self.isDeviceUnLocked == false{
//                    ProgressHUD.show("Proceed to Unlock the Board...")
//                }else{
//                    ProgressHUD.show("Proceed to Lock the Board...")
//                }
                _ = self.connectManager.connect()
                if self.isDeviceUnLocked == false{
                    var helloWorldTimer = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(self.unlockDevice), userInfo: nil, repeats: false)
                }else{
                    var helloWorldTimer = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(self.lockDevice), userInfo: nil, repeats: false)
                }
                
            })
            
            
        }
        )
    }
    @objc func lockDevice()
    {
        ProgressHUD.dismiss()
        //        _ = communicationManager.doAction_Lock()
        CommonMethods.showAlert(withOk: "Proceed to Lock the Board...", andMessage: "", andCallMethodOnOk: #selector(self.dolock), fromObject: self, on: self)
        
        //self.dolock()
        
    }
    @objc func unlockDevice()
    {
         ProgressHUD.dismiss()
        //        _ = communicationManager.doAction_Lock()
        CommonMethods.showAlert(withOk: "Proceed to Unlock the Board...", andMessage: "", andCallMethodOnOk: #selector(self.doUnlock), fromObject: self, on: self)
        
        //self.doUnlock()
        
    }
    @objc func doUnlock(){
        ProgressHUD.show("Unlocking...")
        print(self.isConnected)
        _ = communicationManager.doAction_Unlock()
        print(self.isConnected)
        bleStatusDelegate.statusDelegate(self.isConnected, communicationManager: communicationManager, true)
         ProgressHUD.dismiss()
        CommonMethods.getSnackBarWithMessage(message: "Board is Unlocked successfully")
        self.navigationController?.popViewController(animated: true)
    }
    @objc func dolock(){
        ProgressHUD.show("locking...")
        print(self.isConnected)
        _ = self.communicationManager.doAction_Lock()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
            print(self.isConnected)
            self.bleStatusDelegate.statusDelegate(self.isConnected, communicationManager: self.communicationManager, false)
             ProgressHUD.dismiss()
            CommonMethods.getSnackBarWithMessage(message: "Board is Locked successfully")
            self.navigationController?.popViewController(animated: true)
        }
        
        
    }
    
    func linka_discover_delegate_instance() -> AnyObject {
        
        return self
    }
    
    func onDiscoverNewPeripheral(discoveredPeripheral: DiscoveredPeripherals) {
        var connected : Bool = false
        for linka in connected_linkas {
            if linka.getPeripheral().identifier.uuidString == discoveredPeripheral.peripheral.identifier.uuidString {
                // omit connected devices
                connected = true
            }
        }
        
        if (!connected) {
            var isUnique = true
            for p in list {
                if p === discoveredPeripheral {
                    isUnique = false
                }
            }
            
            if isUnique {
                self.list.append(discoveredPeripheral)
                let linka = Linka.makeLinka(discoveredPeripheral)
                self.linkaList.append(linka)
                
                // Update the UI on the main thread
                DispatchQueue.main.async(execute: {
                    self.loadingView.isHidden = true
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    //***************************** After Pair-up Device *************************//
    
    func communactingMangerConfig(){
        
        connectManager.callback = { (peripheral, error) -> Void in
            
            if (error == nil) {
                self.isConnected = true
            } else {
                self.isConnected = false
            }
            self.refresh()
        }
        
        connectManager.callbackOnLostConnection = { (peripheral, error) -> Void in
            self.isConnected = false
            self.refresh()
        }
        
        connectManager.callbackOnReadRSSI = { (peripheral, RSSI) -> Void in
            if RSSI != nil {
                self.RSSI = RSSI!.intValue
            }
            
            self.refresh()
        }
        
        
        communicationManager.onFetchLockStatusPacket = { (peripheral, lockStatusPacket, error) -> (Void) in
            
            self.lockStatusPacket = lockStatusPacket
            
            if (lockStatusPacket != nil) {
                if lockStatusPacket!.GetTransitionReason() == StateTransitionReason.REASON_STALL {
                    if lockStatusPacket!.GetTransitionReasonPrev() != lockStatusPacket!.GetTransitionReason() {
                        // We stalled... don't let rental end yet
                    }
                }
                if (lockStatusPacket!.GetLockState() == LockState.lock_LOCKED) {
                    // Lock is Locked
                }
                if (lockStatusPacket!.GetLockState() == LockState.lock_UNLOCKED) {
                    // Lock is Unlocked
                }
            }
            
            self.refresh()
        }
        
        communicationManager.onFetchLockSettingPacket = { (peripheral, lockSettingPacket, error) -> (Void) in
            
            if lockSettingPacket!.settingIndex() == UInt(LockSettingPacket.VLSO_SETTING_AUDIO) {
                let audioText = "\(lockSettingPacket!.value())"
                print(audioText)
            }
            
            if lockSettingPacket!.settingIndex() == UInt(LockSettingPacket.VLSO_SETTING_LOCK_PAC_CODE) {
                let pacText = "\(lockSettingPacket!.value())"
                print(pacText)
            }
            
            if lockSettingPacket!.settingIndex() == UInt(LockSettingPacket.VLSO_SETTING_LOCKED_SLEEP_S) {
                let lockSleepText = "\(lockSettingPacket!.value())"
                print(lockSleepText)
            }
            
        }
        
        communicationManager.onFetchLockContextPacket = { (peripheral, lockContextPacket, error) -> (Void) in
            self.lockContextPacket = lockContextPacket
        }
        
        communicationManager.onFetchLockInfoPacket = { (peripheral, lockInfoPacket, error) -> (Void) in
            
            self.lockInfoPacket = lockInfoPacket
            
        }
        
        communicationManager.onFetchLockTamperWarning = { (peripheral, lockStatusPacket, error) -> (Void) in
            
            UIAlertView(title: "Tamper Warning", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
            
        }
        
        communicationManager.onFetchLockBatteryLowWarning = { (peripheral, lockStatusPacket, error) -> (Void) in
            
            UIAlertView(title: "Battery Low Warning", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
            
        }
        
        communicationManager.onFetchLockBatteryCriticallyLowWarning = { (peripheral, lockStatusPacket, error) -> (Void) in
            
            UIAlertView(title: "Battery Critically Low Warning", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
            
        }
        
        communicationManager.onLockSettled = { () -> (Void) in
            
            // Lock is ready for user commands now
            
        }
    }
    
    func refresh() {
        
        var status = ""
        
        status = status + "Connection status: \(self.isConnected ? "CONNECTED" : "DISCONNECTED")"
        status = status + "\nRSSI: \(RSSI)"
        
        let lockContextData = lockContextPacket
        let lockStatusData = lockStatusPacket
        
        if lockStatusData != nil {
            
            status = status + "\n\nlockStatusData - LockState: \(lockStatusData!.GetLockStateRepresentation()), Reason: \(lockStatusData!.GetTransitionReasonLocalized()) \n\nAuthState: \(lockStatusData!.GetAuthStateRepresentation()), \nBatteryPercent: \(lockStatusData!.GetBatteryPercent()), \nBatteryPercentRaw: \(lockStatusData!.GetBatteryPercentRaw()), BatteryVoltage: \(lockStatusData!.GetBatteryVoltage()), StateFlags: \(lockStatusData!.GetStateFlags()), \(lockStatusData!.GetStateFlagsRepresentation()), StatusFlags: \(lockStatusData!.GetStatusFlags()), TamperState: \(lockStatusData!.GetTamperState()), Stall: \(lockStatusData!.GetCurrent_mA())mA"
        }
        
        
        if lockContextData != nil {
            
            status = status + "\n\nlockContextData - encVer: \(lockContextData!.getEncVer()), counter: \(lockContextData!.getCounter())"
        }
        
        status = status + "\n\n"
        status = status + communicationManager.output
        
        let statusText = status
        print(statusText)
    }
    //MARK:- CBCentralManagerDelegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            self.getApiAndSecretKey()
            break
        case .poweredOff:
            print("Bluetooth is Off.")
            showAlertForBLEEnable()
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unsupported:
            break
        case .unknown:
            break
        default:
            break
        }
    }
    
    func showAlertForBLEEnable(){
        let alertController = UIAlertController (title: "One Rent", message: "Please turn on your bluetooth", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Ok", style: .default) { (_) -> Void in
            self.openBluetooth()
        }
        alertController.addAction(settingsAction)
        present(alertController, animated: true, completion: nil)
    }
    func openBluetooth(){
//        let url = URL(string: "App-Prefs:root=Bluetooth") //for bluetooth setting
//        let app = UIApplication.shared
//        app.open(url!, options: [:], completionHandler: nil)
//        
        let settingsUrl = NSURL(string: UIApplication.openSettingsURLString)
        if let url = settingsUrl {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
}
