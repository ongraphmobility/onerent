//
//  HowWorksViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 16/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class HowWorksViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var worksTablView: UITableView!
    @IBOutlet weak var bottomTaglineLabel:UILabel!
    @IBOutlet weak var backBtn:UIButton!
    
    var dynamicModel : [DynamicModel]!
    var isfromSplash : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomTaglineLabel.text = DynamicUserDataUtility.getInstance().agreement_age_text
        worksTablView.estimatedRowHeight = 135
        worksTablView.rowHeight = UITableView.automaticDimension
        worksTablView.separatorStyle = .none
        worksTablView.delegate = self
        worksTablView.dataSource = self
        worksTablView.reloadData()
        CommonMethods.setStatusBarBackgroundColor(color: .black)
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    override func viewWillAppear(_ animated: Bool) {
        if isfromSplash{
            backBtn.isHidden = true
        }else{
            backBtn.isHidden = false
        }
    }
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DEFINED_ARRAYS.howWorksTitleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorksTableViewCell", for: indexPath) as! WorksTableViewCell
        cell.selectionStyle = .none
        cell.lblTitle?.text = DEFINED_ARRAYS.howWorksTitleArr.object(at: indexPath.row) as? String
        cell.lblDesc?.text = DEFINED_ARRAYS.howWorksDescpArr.object(at: indexPath.row) as? String
        cell.imgView?.image = UIImage.init(named: DEFINED_ARRAYS.howWorksimgArr.object(at: indexPath.row) as! String)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    @IBAction func closeBtnActn(_ sender : UIButton){
        if isfromSplash{
          moveUserToLogin()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func moveUserToLogin(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
       // let appWindow = UIApplication.shared.delegate?.window!
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        let navigatioStack = UINavigationController(rootViewController: viewController)
//        navigatioStack.navigationBar.isHidden = true
//        appWindow!.rootViewController = navigatioStack
//        appWindow!.makeKeyAndVisible()
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
