//
//  PaymentsViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 12/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class PaymentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CardIOPaymentViewControllerDelegate {

    @IBOutlet weak var paymentTablView: UITableView!
    @IBOutlet weak var pricingLabel:UILabel!
    var dictPayData = NSMutableDictionary()
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var subHeadingLbl: UILabel!
   
    var userCCModel : UserCCInfoModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pricingLabel.text = DynamicUserDataUtility.getInstance().pricing
        //CommonMethods.setStatusBarBackgroundColor(color: .black)
        
        guard let isSkip = CommonMethods.getValueFromUserDefaults(key: STRINGS.ISPAYMENTSKIP)else{
            return
        }
            
        if isSkip == "true"{
            skipBtn.isHidden = false
            backBtn.isHidden = true
        }else{
            skipBtn.isHidden = true
            backBtn.isHidden = false
            
        }
        setupDefaults()
        CardIOUtilities.preload()
        self.subHeadingLbl.text = "ADD PAYMENT METHOD"
    }
    override func viewDidAppear(_ animated: Bool) {
        callApiForGetCCInfo()
        guard let isComingFrmUnlock = CommonMethods.getValueFromUserDefaults(key: STRINGS.ISFROMUNLOCK)else{
            return
        }
        if isComingFrmUnlock == "true"{
            self.subHeadingLbl.text = "PAYMENT METHOD"
        }
    }
    func setupDefaults(){
        paymentTablView.delegate = self
        paymentTablView.dataSource = self
        paymentTablView.reloadData()
    }
    
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DEFINED_ARRAYS.paymentMethodArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = DEFINED_ARRAYS.paymentMethodArray.object(at: indexPath.row) as? String
        cell.imageView?.image = UIImage.init(named: DEFINED_ARRAYS.paymentMethodImageArray.object(at: indexPath.row) as! String)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let addCardViewControllerObj: AddCardViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.ADDCARD) as? AddCardViewController
            if self.userCCModel.data != nil{
                if (self.userCCModel.data?.count)! > 0{
                     addCardViewControllerObj?.userCCInfoArray = self.userCCModel.data
                }
            }
            self.navigationController?.pushViewController(addCardViewControllerObj!, animated: true)
        }else{
            let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
            cardIOVC?.collectCardholderName = false
            cardIOVC?.hideCardIOLogo = true
            cardIOVC?.guideColor = UIColor.green
            cardIOVC?.collectCVV = true
            present(cardIOVC!, animated: true, completion: nil)
        }
    }

    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }

    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        if let info = cardInfo {
            let str = NSString(format: "Received card info. \n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            print(str)
            self.getToken(info: info, paymentViewController: paymentViewController)
        }
    }
    
    
    
    private func getToken(info : CardIOCreditCardInfo!, paymentViewController: CardIOPaymentViewController!){
        ProgressHUD.show("Payment processing..")
        let cardParams = STPCardParams()
        cardParams.number = info.cardNumber
        cardParams.expMonth = info.expiryMonth
        cardParams.expYear = info.expiryYear
        cardParams.cvc = info.cvv
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                return
            }
            
            self.dictPayData["stripe_token"] = token.tokenId
            print(self.dictPayData)
            self.callApiForPayment(paymentViewController: paymentViewController)
            
        }
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func skipBtnActn(_ sender : UIButton){
        
        pushToHome()
    }
    func pushToHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigatioStack = UINavigationController(rootViewController: vc)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }
    
    func callApiForGetCCInfo(){
         let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.userManagemnet, showLoading: true) { (true, result) in
            
            if let isResult = result{
                do {
                    self.userCCModel = try JSONDecoder().decode(UserCCInfoModel.self, from: isResult)
                    DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "ENTER CREDIT CARD")
                    if self.userCCModel.status == "true" {
                        if self.userCCModel.data != nil{
                            if (self.userCCModel.data?.count)! > 0{
                                guard let isComingFrmUnlock = CommonMethods.getValueFromUserDefaults(key: STRINGS.ISFROMUNLOCK)else{
                                    return
                                }
                                if isComingFrmUnlock == "true"{
                                   DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "MAKE PAYMENT")
                                }else{
                                   DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "CHANGE OR DELETE CREDIT CARD")
                                }
                                self.paymentTablView.reloadData()
                            }else{
                                DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "ENTER CREDIT CARD")
                            }
                        }
                    }else{
                        guard let isComingFrmUnlock = CommonMethods.getValueFromUserDefaults(key: STRINGS.ISFROMUNLOCK)else{
                            return
                        }
                        if isComingFrmUnlock == "true"{
                            DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "MAKE PAYMENT")
                        }else{
                           DEFINED_ARRAYS.paymentMethodArray.replaceObject(at: 0, with: "ENTER CREDIT CARD")
                        }
                        self.paymentTablView.reloadData()
                    }
                    self.paymentTablView.reloadData()
                }catch (let error){
                    print(error)
                }
            }
           self.paymentTablView.reloadData()
        }
    }
    func callApiForPayment(paymentViewController: CardIOPaymentViewController!){
        
        let param: Parameters = ["amount": 110, "stripe_token": self.dictPayData.value(forKey: "stripe_token") as Any]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.payment, showLoading: false) { (true, result) in
            
            if let isResult = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
                    ProgressHUD.dismiss()
                    if loginResp.success == "True" {
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        paymentViewController.dismiss(animated: true, completion: {
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                CommonMethods.saveValueInUserDefaults(Value: "true", forKey: STRINGS.ISFROMUNLOCK)
                                UserDefaults.standard.set(true, forKey: STRINGS.IS_TRANSACTION_SUCCESS)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                        })
                    }else{
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }

}
