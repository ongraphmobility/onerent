//
//  ContactUsViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 14/02/19.
//  Copyright © 2019 Ongraph Technology. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var callUsBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var subjectTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var messegeTxtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messegeTxtView.delegate = self
    }
    
    func linkCallBtn(){
        let number = "7275924001"
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    //MARK:- UITextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        print(textView.text)
        if textView.text == "Enter Message..."{
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text?.isEmpty)!{
            textView.text = "Enter Message..."
        }
    }
    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBtnActn(_ sender : UIButton){
        if firstNameTxt.text?.isEmpty == true && lastNameTxt.text?.isEmpty == true && emailTxt.text?.isEmpty == true && subjectTxt.text?.isEmpty == true && messegeTxtView.text.isEmpty == true {
            CommonMethods.getSnackBarWithMessage(message: "Please submit the details")
            return
        }
        if emailTxt.text?.isEmpty == true{
            CommonMethods.getSnackBarWithMessage(message: "Please enter your email id")
            return
        }
        if CommonMethods.isValidEmail(testStr: emailTxt.text!){
            self.apiCallForContactUs()
        }else{
            CommonMethods.getSnackBarWithMessage(message: "Please enter a valid email id")
            return
        }
    }
    @IBAction func callUsBtnActn(_ sender : UIButton){
        linkCallBtn()
    }
    // MARK: API Call for Contact Us
    func apiCallForContactUs(){
        let header : HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        let param: Parameters = ["first_name": firstNameTxt.text ?? "","last_name": lastNameTxt.text ?? "","email": emailTxt.text!,"subject": subjectTxt.text ?? "","enter_message": messegeTxtView.text ?? ""]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.contactUs, showLoading: true) { (false, result) in
            if let results = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
                    if loginResp.success == "True" {
                        CommonMethods.showAlert(withTitle: "Thank You", alertMessage: "We will respond back as soon as possible.", sender: self)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}

