//
//  BoardDetailViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 30/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class BoardDetailViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var boardWebView:UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    var urlStr = String()
    var titleStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        //Document file url
        let docUrl = NSURL(string: AppUrl.BASE_URL + urlStr.dropFirst())
        
        let req = NSURLRequest.init(url: docUrl! as URL)
        boardWebView.delegate = self
        //here is the sole part
        boardWebView.scalesPageToFit = true
        boardWebView.scrollView.showsVerticalScrollIndicator = false
        boardWebView.contentMode = .scaleAspectFit
        boardWebView.loadRequest(req as URLRequest)
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        CommonMethods.getLabelFontSizesWithDevices(label: titleLabel)
        titleLabel.text = titleStr
    }

}
