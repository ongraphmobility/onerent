//
//  ProfileViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 01/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {

     @IBOutlet weak var imageProfile: UIImageView!
     @IBOutlet weak var txtFieldName: UITextField!
     @IBOutlet weak var txtFieldEmail: UITextField!
     @IBOutlet weak var txtFieldPhone: UITextField!
     @IBOutlet weak var btnVerify: UIButton!
     @IBOutlet weak var btnLogout: UIButton!
    let picker = UIImagePickerController()
    var choosenImg = UIImage()
    var userProfileModal : UserProfileModal!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // CommonMethods.setStatusBarBackgroundColor(color: .black)
        callApiForGetUpdateUserProfile()
        setupViews()
    }
    func setupViews(){
        btnVerify.layer.cornerRadius = 10.0
        btnVerify.layer.masksToBounds = true
        btnVerify.layer.borderColor = UIColor.red.cgColor
        btnVerify.layer.borderWidth = 2.0
        btnLogout.layer.cornerRadius = 3.0
        btnLogout.layer.masksToBounds = true
        
        imageProfile.layer.masksToBounds = false
        imageProfile.layer.cornerRadius = imageProfile.frame.height/2
        imageProfile.clipsToBounds = true
        imageProfile.layer.borderColor = UIColor.lightGray.cgColor
        imageProfile.layer.borderWidth = 1.0
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageProfile.isUserInteractionEnabled = true
        imageProfile.addGestureRecognizer(tapGestureRecognizer)
        
        if let email = CommonMethods.getValueFromUserDefaults(key: STRINGS.USER_EMAIL) as? String {
            txtFieldEmail.text = email
        }
        if let isVerified = UserDefaults.standard.value(forKey: STRINGS.IS_USER_VERIFIED) as? Bool {
            if isVerified{
                btnVerify.isHidden = true
            }else{
                btnVerify.isHidden = false
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnActn(_ sender : UIButton){
        if txtFieldName.text?.isEmpty == true{
         CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter your name", sender: self)
            return
        }
        if txtFieldEmail.text?.isEmpty == true{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter your email", sender: self)
            return
        }
        if CommonMethods.isValidEmail(testStr: (txtFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines))!) == false{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_EMAIL_ERROR, sender: self)
            return
        }
        if txtFieldPhone.text?.isEmpty == true{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter your phone number", sender: self)
            return
        }
        if imageProfile.image == UIImage.init(named: "profile_img_iPhone"){
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please Choose an image", sender: self)
            return
        }else{
            //self.callApiForGetUpdateUserProfile()
            self.callApisaveUpdateUserProfile()
        }
       
    }
    @IBAction func verifyBtnActn(_ sender : UIButton){
        if (txtFieldEmail.text?.count)! > 0 && CommonMethods.isValidEmail(testStr: txtFieldEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
           callApiForVerifyEmail(txt: txtFieldEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        }
        
    }
    @IBAction func rentalAgreementBtnActn(_ sender : UIButton){
        if let url = URL(string: AppUrlKey.aggreementPolicy) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    @IBAction func tosBtnActn(_ sender : UIButton){
        if let url = URL(string: AppUrlKey.termsOfService) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    @IBAction func ppBtnActn(_ sender : UIButton){
        if let url = URL(string: AppUrlKey.privacyPolicy) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    @IBAction func logOutBtnActn(_ sender : UIButton){
        configureLogOut()
    }
    
    //Mark: profile image View delegate & Setup
    //MARK: IMAGEPICKER METHOD
    func showPhotoChooseOptions(){
        let alert = UIAlertController(title: nil, message: "Choose your option", preferredStyle: UIAlertController.Style.actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Camera selected")
            self.openCamera()
        })
        alert.addAction(UIAlertAction(title: "Photos", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Photo selected")
            self.openGallery()
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("cancel")
            alert.dismiss(animated: true, completion: nil)
        })
        present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.delegate = self
            
            picker.modalPresentationStyle = .fullScreen
            self.present(picker,animated: true,completion: nil)
        }else{
            noCamera()
        }
    }
    
    func openGallery(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(picker, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        self.present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    //MARK: IMAGEPICKER DELEGATES
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        choosenImg = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageProfile.image = choosenImg
        self.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        showPhotoChooseOptions()
    }
    
    //MARK: Api call for verify email id
    func callApiForVerifyEmail(txt: String){
        let param: Parameters = ["email": txt]
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.verfifyEmail, showLoading: true) { (true, result) in
            
            if let results = result{
                do {
                    let dataString = try CommonMethods.convertDataIntoString(data: results)
                    let jsonDict = CommonMethods.convertToDictionary(text: dataString)
                    CommonMethods.getSnackBarWithMessage(message: (jsonDict!["message"] as? String)!)
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    
    //MARK:- Api call for get user profile
    func callApiForGetUpdateUserProfile(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.userProflie, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.userProfileModal = try JSONDecoder().decode(UserProfileModal.self, from: results)
                    if self.userProfileModal != nil{
                            CommonMethods.saveValueInUserDefaults(Value: (self.userProfileModal.email ?? ""), forKey: STRINGS.USER_EMAIL)
                            CommonMethods.saveValueInUserDefaults(Value: (self.userProfileModal.name ?? ""), forKey: STRINGS.USER_FULLNAME)
                            CommonMethods.saveValueInUserDefaults(Value: (self.userProfileModal.name ?? ""), forKey: STRINGS.USER_PHONENUMBER)
                            CommonMethods.saveValueInUserDefaults(Value: (self.userProfileModal.profile_picture ?? ""), forKey: STRINGS.USER_PROFILE_PICTURE)
                            UserDefaults.standard.set(self.userProfileModal.verified, forKey: STRINGS.IS_USER_VERIFIED)
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set(self.userProfileModal.id, forKey: STRINGS.USER_ID)
                            UserDefaults.standard.synchronize()
                        DispatchQueue.main.async {
                            self.updateProfieData()
                        }
                        
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    func updateProfieData(){
        self.txtFieldPhone.text = self.userProfileModal.phone_number
        self.txtFieldName.text = self.userProfileModal.name
        self.txtFieldEmail.text = self.userProfileModal.email
        if (self.userProfileModal.profile_picture) != nil{
            let url = URL(string: (self.userProfileModal.profile_picture)!)
            imageProfile.sd_setImage(with: url, placeholderImage: UIImage.init(named: "profile_img_iPhone"), options: [SDWebImageOptions.refreshCached,SDWebImageOptions.highPriority,SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                if error != nil {
                    //                    self.img_userImage.image = #imageLiteral(resourceName: "user_icon_iPhone")
                } else{
                    self.imageProfile.image = image
                }
            }
        }
    }
    //verification of fields
    func isValidCredentials() -> Bool{
        if(imageProfile.image == nil){
           CommonMethods.showAlert(withTitle: "Error", alertMessage: "Please select an image", sender: self)
            return false
        }
        return true
    }
    
    
    func callApisaveUpdateUserProfile(){
        self.view.endEditing(true)
//        var phoneCode = ""
//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCode)
//            phoneCode = CommonMethods.getCountryPhonceCode(countryCode)
//        }
        if(isValidCredentials() == true){
            ProgressHUD.show("Loading..")
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 240
            let profileImageData = imageProfile.image?.jpegData(compressionQuality: 0.3)
            let param: [String : String] = ["email": txtFieldEmail.text!,
                                            "name": txtFieldName.text!,
                                           // "phone_number": txtFieldPhone.text!.contains(phoneCode) ? txtFieldPhone.text! : phoneCode + txtFieldPhone.text!,
                                            "phone_number": txtFieldPhone.text!
                                            ]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        Alamofire.upload(multipartFormData:{ (multipartFormData) in
            multipartFormData.append(profileImageData!, withName: "profile_picture", fileName: "image.jpg", mimeType: "image/png")
            
            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                             to:AppUrl.BASE_URL + AppApis.userProflie,
                             method:.patch,
                             headers:headerParams,
                             encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        debugPrint(response)
                                        ProgressHUD.dismiss()
                                        print(response)
                                        if let JSON = response.result.value {
                                            let responseDataDict = JSON as! NSDictionary
                                            print(responseDataDict)
                                            if let phnNum = responseDataDict.value(forKey: "phone_number") as? NSArray{
                                                if (phnNum[0] as AnyObject).contains("not valid") == true{
                                                    CommonMethods.getSnackBarWithMessage(message: "The phone number entered is not valid.")
                                                }else{
                                                    CommonMethods.getSnackBarWithMessage(message: "Your profile has been updated.")
                                                }
                                            }
                                            if let phnNum = responseDataDict.value(forKey: "phone_number") as? String{
                                                if phnNum.contains("not valid") == true{
                                                    CommonMethods.getSnackBarWithMessage(message: "The phone number entered is not valid.")
                                                }else{
                                                    CommonMethods.getSnackBarWithMessage(message: "Your profile has been updated.")
                                                }
                                            }
                                        }
                                    }
                                case .failure(let encodingError):
                                    print(encodingError)
                                }
            })
        }
    }
    
    //MARK: Api call for log out
    func callApiForLogOutUser(){
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: nil, apiName: AppApis.logout, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    let dataString = try CommonMethods.convertDataIntoString(data: results)
                    let dict = CommonMethods.convertToDictionary(text: dataString)
                    let appDomain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: appDomain)
                    UserDefaults.standard.synchronize()
                    
                    let detail = dict!["detail"] as! String
                    if detail == "Successfully logged out."{
                        CommonMethods.moveToLogin()
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }

    func configureLogOut(){
        if CommonMethods.getValueFromUserDefaults(key: STRINGS.USER_TYP_KEY) == STRINGS.LOGGEDIN_USER{
            let alert:UIAlertController=UIAlertController(title: MESSAGES.LOGOUT_MSG, message: nil, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: STRINGS.OK, style: UIAlertAction.Style.default){
                UIAlertAction in
                var navControllers = self.navigationController?.viewControllers
                navControllers?.removeAll()
                self.callApiForLogOutUser()
            }
            let cancelAction = UIAlertAction(title: STRINGS.CANCEL, style: UIAlertAction.Style.cancel){
                UIAlertAction in
            }
            // Add the actions
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }

}
