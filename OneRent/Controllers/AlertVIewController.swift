//
//  AlertVIewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 10/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

protocol CustomAlertViewDelegate: class {
    func okButtonTapped(nameTextFieldValue: String, emailTextFieldValue: String)
    func cancelButtonTapped()
}

class AlertVIewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: CustomAlertViewDelegate?
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    override func viewDidLoad() {
        super.viewDidLoad()
         emailTextField.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()

    }
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onTapCancelButton(_ sender: Any) {
        emailTextField.resignFirstResponder()
        nameTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapSaveButton(_ sender: Any) {
        emailTextField.resignFirstResponder()
        nameTextField.resignFirstResponder()
        if CommonMethods.isValidEmail(testStr: emailTextField.text!) == false {
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_EMAIL_ERROR, sender: self)
            return
        }
        if nameTextField.text?.isEmpty == true{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter your name", sender: self)
            return
        }
        delegate?.okButtonTapped(nameTextFieldValue: nameTextField.text!, emailTextFieldValue: emailTextField.text!)
        CommonMethods.getSnackBarWithMessage(message: "Rider Added Sucessfully")
        self.dismiss(animated: true, completion: nil)
    }


}
