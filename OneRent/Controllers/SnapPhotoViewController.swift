//
//  SnapPhotoViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 20/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit
import AVFoundation

//protocol SnapPhotoDelegate {
//
//    func getSnapPhotoDelegate(_ image: UIImage)
//
//}

class SnapPhotoViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCapturePhotoCaptureDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var topbar: UIView!
    @IBOutlet weak var snapPhotoView: UIView!
    @IBOutlet weak var takePictureBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    let picker = UIImagePickerController()
    var choosenImg = UIImage()
//    var snapPicDelegate : SnapPhotoDelegate?
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCameraConfig()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    func setupCameraConfig(){
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
        }
        DispatchQueue.main.async {
            self.videoPreviewLayer.frame = self.snapPhotoView.bounds
        }
    }
    func setupLivePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        snapPhotoView.layer.addSublayer(videoPreviewLayer)
    }

    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?){
        if let error = error {
            print(error.localizedDescription)
        }
        
        if let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            print(UIImage(data: dataImage)!.size)
            //snapPicDelegate?.getSnapPhotoDelegate(UIImage(data: dataImage)!)
            choosenImg = UIImage(data: dataImage)!
            callApiSubmitDamageReport()
            self.navigationController?.popViewController(animated: true)
        } else {
            
        }
    }
    

    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func takePhotoBtnActn(_ sender : UIButton){
//        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        //stillImageOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
        capturePhoto()
    }
    func capturePhoto() {
        
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
                             ]
        settings.previewPhotoFormat = previewFormat
        self.stillImageOutput.capturePhoto(with: settings, delegate: self)
        
    }
    
    //MARK: Api call for Submit Report
    func callApiSubmitDamageReport()
    {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 240
        let reportImageData = self.choosenImg.jpegData(compressionQuality: 0.3)
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        Alamofire.upload(multipartFormData:{ (multipartFormData) in
            multipartFormData.append(reportImageData!, withName: "image", fileName: "image.jpg", mimeType: "image/png")
        },
                         to:AppUrl.BASE_URL + AppApis.damageImage,
                         method:.post,
                         headers:headerParams,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    debugPrint(response)
                                    ProgressHUD.dismiss()
                                    print(response)
                                    if let JSON = response.result.value {
                                        let responseDataDict = JSON as! NSDictionary
                                        print(responseDataDict)
                                        if responseDataDict.value(forKey: "success")  as? String == "True"{
                                            let message = responseDataDict.value(forKey: "message")  as? String
                                            CommonMethods.getSnackBarWithMessage(message: message!)
                                        }
                                    }
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        })
    }
    
}
