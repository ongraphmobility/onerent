//
//  NumericViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 17/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class NumericViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var boardImg:UIView!
    @IBOutlet weak var codeTextField: UITextField!
    var qrScanModel : QRScanModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //CommonMethods.setStatusBarBackgroundColor(color: .black)
        CommonMethods.getLabelFontSizesWithDevices(label: titleLabel)
        codeTextField.delegate = self
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
        if textField.text?.isEmpty == true{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter valid code", sender: self)
            return
        }
        self.callApiForQRScanDetails()
    }
    //MARK: Api call for QRScanDetails
    func callApiForQRScanDetails(){
        let param: Parameters = ["linka_lock_id": codeTextField.text!]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.qrScan, showLoading: true) { (true, result) in
            
            if let results = result{
                do {
                     self.qrScanModel = try JSONDecoder().decode(QRScanModel.self, from: results)
                    if self.qrScanModel.status == "True"{
                        self.view.window!.rootViewController?.dismiss(animated: true, completion: {
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                CommonMethods.saveValueInUserDefaults(Value: self.codeTextField.text!, forKey: STRINGS.LINKA_LOCK_ID)
                                UserDefaults.standard.set(true, forKey: STRINGS.IS_BOARD_AACTIVE)
                                UserDefaults.standard.set(results, forKey: STRINGS.QRSCAN_MODEL_DATA)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                        })
                    }else{
                        CommonMethods.getSnackBarWithMessage(message: "Invalid code OR Board is already in use")
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}
