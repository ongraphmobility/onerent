//
//  ReportDamageViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 19/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class ReportDamageViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var reportTableView: UITableView!
    @IBOutlet weak var noDamageBtn: UIButton!
    var reportModel : ReportModel!
    var reportDataArray : [ReportData]!
    var selectedCheckFirstSection = Set<Int>()
    var selectedCheckSecondSection = Set<Int>()
    var majorArray = NSMutableArray()
    var minorArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GlobalTimer.getInstance().seconds = 0
        GlobalTimer.getInstance().startTimer()
        print(GlobalTimer.getInstance().seconds)
        self.reportTableView.isHidden = true
        setupViews()
        callApiForReport()
    }
    func setupViews(){
        noDamageBtn.layer.cornerRadius = 5.0
        noDamageBtn.layer.borderColor = UIColor.black.cgColor
        noDamageBtn.layer.borderWidth = 1.0
    }
    
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func snapPhotoBtnActn(_ sender : UIButton){
       pushToSnapVc()
    }
    @IBAction func submitBtnActn(_ sender : UIButton){
//        if self.damageImage != nil{
        if selectedCheckFirstSection.count > 0 || selectedCheckSecondSection.count > 0{
            let majorDict = NSMutableDictionary()
            let minorDict = NSMutableDictionary()
            for i in selectedCheckFirstSection{
                majorDict.setValue(self.reportDataArray[0].major![i], forKey: "text")
                majorDict.setValue("True", forKey: "value")
                majorArray.add(majorDict)
            }
            for j in selectedCheckSecondSection{
                minorDict.setValue(self.reportDataArray[0].minor![j], forKey: "text")
                minorDict.setValue("True", forKey: "value")
                minorArray.add(minorDict)
            }
            GlobalTimer.getInstance().stopTimer()
            TIME.after_time = GlobalTimer.getInstance().seconds
            UserDefaults.standard.set(TIME.after_time, forKey: STRINGS.TIME_AFTER)
            UserDefaults.standard.synchronize()
            self.apiCallForSubmitReportData(isDamage: "True")
        }else{
            CommonMethods.getSnackBarWithMessage(message: "Please mark the selection.")
            return
        }
    }
    @IBAction func noDamageBtnActn(_ sender : UIButton){
        self.apiCallForSubmitReportData(isDamage: "False")
    }
    func pushToSnapVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SnapPhotoViewController") as! SnapPhotoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Api call for Get Report
    func callApiForReport(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.report, showLoading: true) { (true, result) in
            
            if let results = result{
                do {
                    self.reportModel = try JSONDecoder().decode(ReportModel.self, from: results)
                    self.reportDataArray = self.reportModel.data
                    self.footerLabel.text = self.reportDataArray[0].footer
                    self.reportTableView.delegate = self
                    self.reportTableView.dataSource = self
                    self.reportTableView.separatorStyle = .none
                    self.reportTableView.isHidden = false
                    self.reportTableView.reloadData()
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    // MARK: API Call for Report
    func apiCallForSubmitReportData(isDamage : String){
        let param : [String : Any] = [
                                    "damage": isDamage,
                                    "major": self.majorArray,
                                    "minor": self.minorArray
                                     ]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.damageReport, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
                    if loginResp.success == "True"{
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            GlobalTimer.getInstance().stopTimer()
                            TIME.after_time = GlobalTimer.getInstance().seconds
                            UserDefaults.standard.set(TIME.after_time, forKey: STRINGS.TIME_AFTER)
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set(true, forKey: STRINGS.IS_REPORT_SUBMITTED)
                            UserDefaults.standard.synchronize()
                            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                        }
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}

extension ReportDamageViewController : UITableViewDelegate, UITableViewDataSource{
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if (self.reportDataArray[0].major?.count)! > 0 && (self.reportDataArray[0].minor?.count)! > 0{
           return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.reportDataArray[0].major?.count ?? 0
        default:
            return self.reportDataArray[0].minor?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "ReportHeaderCell") as? WaiverTableCell
        switch section {
        case 0:
            headerCell?.lblTitle.text = "Major"
        default:
            headerCell?.lblTitle.text = "Minor"
        }
        headerView.addSubview(headerCell!)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WaiverTableCell", for: indexPath) as! WaiverTableCell
        cell.selectionStyle = .none
        switch indexPath.section {
        case 0:
            cell.lblTitle?.text = self.reportDataArray[0].major![indexPath.row]
            if selectedCheckFirstSection.contains(indexPath.row){
                cell.checkUncheckImage.image = UIImage.init(named: "checkbox_blue_iPhone")
            }else{
                cell.checkUncheckImage.image = UIImage.init(named: "checkbox_white_iPhone")
            }
        default:
            cell.lblTitle?.text = self.reportDataArray[0].minor![indexPath.row]
            if selectedCheckSecondSection.contains(indexPath.row){
                cell.checkUncheckImage.image = UIImage.init(named: "checkbox_blue_iPhone")
            }else{
                cell.checkUncheckImage.image = UIImage.init(named: "checkbox_white_iPhone")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if selectedCheckFirstSection.contains(indexPath.row){
                selectedCheckFirstSection.remove(indexPath.row)
            }else{
                selectedCheckFirstSection.insert(indexPath.row)
            }
            self.reportTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        default:
            if selectedCheckSecondSection.contains(indexPath.row){
                selectedCheckSecondSection.remove(indexPath.row)
            }else{
                selectedCheckSecondSection.insert(indexPath.row)
            }
            self.reportTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        }
        
    }
}
