//
//  PaddleRecapViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 23/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class PaddleRecapViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var rideTimeLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    
    var feedbackModel : FeedbackModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCallForGetFeedbackData()
    }

    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtnActn(_ sender : UIButton){
       if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
        (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
    }
    
    func setModelData(dataModel : FeedbackModel){
            self.rideTimeLabel.text = dataModel.ride_Time
        self.costLabel.text = String(format : "$ %.2f", Double(dataModel.extra_time_amount!)! + Double(dataModel.ride_time_amount!)!)
    }
    // MARK: API Call for Get FeedBack
    func apiCallForGetFeedbackData(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.feedback, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.feedbackModel = try JSONDecoder().decode(FeedbackModel.self, from: results)
                    self.setModelData(dataModel: self.feedbackModel)
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}
