//
//  VerifyViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class VerifyViewController: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet var emailTxtLabel: UILabel!
    @IBOutlet var openEmailTxtLabel: UILabel!
    @IBOutlet weak var oneLabel: UILabel!
    @IBOutlet var resendEmailBtn: UIButton!
    @IBOutlet var enterCodeBtn: UIButton!
    @IBOutlet weak var twoLabel: UILabel!
    var emailString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // CommonMethods.setStatusBarBackgroundColor(color: .black)
        setupView()
    }
    func setupView(){
        
        oneLabel.layer.cornerRadius = 20.0
        oneLabel.layer.masksToBounds = true
        twoLabel.layer.cornerRadius = 20.0
        twoLabel.layer.masksToBounds = true
        
        resendEmailBtn.layer.cornerRadius = 10.0
        resendEmailBtn.layer.masksToBounds = true
        resendEmailBtn.layer.borderColor = UIColor.white.cgColor
        resendEmailBtn.layer.borderWidth = 2.0
        
        CommonMethods.getLabelFontSizesWithDevices(label: emailTxtLabel)
        CommonMethods.getLabelFontSizesWithDevices(label: openEmailTxtLabel)
        emailTxtLabel.text = "An email was sent to \(emailString) with your magic link to log in"

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(openMailController(_:)))
        emailTxtLabel.isUserInteractionEnabled = true
        tap.delegate = self
        emailTxtLabel.addGestureRecognizer(tap)
    }
    
    @objc func openMailController(_ sender : UITapGestureRecognizer){
        let mailURL = URL(string: "message://")!
        if UIApplication.shared.canOpenURL(mailURL) {
            UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
        }
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendBtnActn(_ sender : UIButton){
         callApiForLogin()
    }
   
     @IBAction func enterCodeBtnActn(_ sender : UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CodeViewController") as! CodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func callApiVerifyLoginCode(code:Int64){
        let param: Parameters = ["code": code]
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.login + AppApis.code, showLoading: true) { (true, result) in
            if let isResult = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
                    if loginResp.success == "true" {
                        if  loginResp.is_new_user == "false"{
                            CommonMethods.saveValueInUserDefaults(Value: loginResp.token!, forKey: STRINGS.TOKEN_KEY)
                            CommonMethods.saveValueInUserDefaults(Value: STRINGS.LOGGEDIN_USER, forKey: "UserTyp")
                            CommonMethods.saveValueInUserDefaults(Value: (loginResp.user?.email ?? ""), forKey: STRINGS.USER_EMAIL)
                            UserDefaults.standard.set(loginResp.user?.verified, forKey: STRINGS.IS_USER_VERIFIED)
                            UserDefaults.standard.synchronize()
                            UserDefaults.standard.set(loginResp.user?.id, forKey: STRINGS.USER_ID)
                            UserDefaults.standard.synchronize()
                            self.pushToHome()
                        }else{
                            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_LOGIN_CODE, sender: self)
                        }
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    
    
    func callApiForLogin(){
        
        let param: Parameters = ["username": emailString]
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.login, showLoading: true) { (true, result) in
            
            if let results = result{
            do {
                let loginResp = try JSONDecoder().decode(LoginResponse.self, from: result!)
                
                if loginResp.success == "true" {
                    if loginResp.message == "Login code has been sent to your email"{
                        CommonMethods.getSnackBarWithMessage(message: MESSAGES.MAIL_SEND)
                    }else{
                       CommonMethods.getSnackBarWithMessage(message: ERRORS.INVALID_LOGIN)
                    }
                    
                }
            }catch (let error){
                print(error)
            }
            }
        }
    }
    
    func pushToHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigatioStack = UINavigationController(rootViewController: vc)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }
}
