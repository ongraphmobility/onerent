//
//  PaddleTipsViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class PaddleTipsViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var borderView:UIView!
    @IBOutlet weak var weatherTxtView:UITextView!
    
    var paddleTipsModel : PaddleTipsModel!
    var paddleTipsDataArray : [PaddleTipsData]!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        apiCallForGetPaddleTipsData()
    }
    func setupViews(){
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 1.0
    }
    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
    func setupData(data: [PaddleTipsData]){
        textView()
        
    }
    func textView(){
        let norString = "CHECK WEATHER OR CURRENTS IN YOUR AREA HERE."
        let attributedString = NSMutableAttributedString(string: norString)
        let linkRange1 = (attributedString.string as NSString).range(of: "WEATHER")
//        attributedString.addAttribute(NSAttributedString.Key.link, value: self.paddleTipsDataArray[0].website_link!, range: linkRange1)
        attributedString.addAttribute(NSAttributedString.Key.link, value: "", range: linkRange1)
        let linkRange2 = (attributedString.string as NSString).range(of: "CURRENTS")
//        attributedString.addAttribute(NSAttributedString.Key.link, value: self.paddleTipsDataArray[0].website_link!, range: linkRange2)
        attributedString.addAttribute(NSAttributedString.Key.link, value: "", range: linkRange2)
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue,
        ]
       // let myAttributedString = NSMutableAttributedString(string: norString, attributes: [NSAttributedString.Key.font:weatherTxtView.font!.withSize(15)])
//        linkAttributes = [ NSAttributedString.Key.font: weatherTxtView.font!.withSize(15) ]
        // textView is a UITextView
        weatherTxtView.linkTextAttributes = linkAttributes
        weatherTxtView.attributedText = attributedString
        weatherTxtView.textColor = UIColor.white
        weatherTxtView.textAlignment = .center
        weatherTxtView.delegate = self
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    // MARK: API Call for Get Paddle Tips Data
    func apiCallForGetPaddleTipsData(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.paddleTips, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.paddleTipsModel = try JSONDecoder().decode(PaddleTipsModel.self, from: results)
                    self.paddleTipsDataArray = self.paddleTipsModel.data
                    if self.paddleTipsDataArray.count > 0{
                        self.setupData(data: self.paddleTipsDataArray)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}
