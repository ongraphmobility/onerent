//
//  HomeViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit
import MapKit


class HomeViewController: UIViewController,SideMenuDelegate,UIGestureRecognizerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UITextViewDelegate,BLEStatusDelegate {
    
    @IBOutlet var sideMenuView: SideMenuView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var paddleBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var availablityLabel: UILabel!
    fileprivate var sideMenuBagroundView = UIView()
    var sideMenudelegate : SideMenuDelegate?
    fileprivate var isSideMenuOpen : Bool = true
    @IBOutlet weak var inventoryView: AvailableInventoryView!
    @IBOutlet weak var myView: GMSMapView!
    var platformList : PlatformListModal!
    var platformInfoWindow : PlatformWindowModal!
    
    var locationManager = CLLocationManager()
    var currentLocationMarker: GMSMarker?
    let customMarkerWidth : Int = 100
    let customMarkerHeight :Int = 80
    var platformDataDict : PlatformData!
    var platformDataArray = NSMutableArray()
    var isUpdateCurrentFirst : Bool = true
    let appDelegate = UIApplication.shared.delegate
    var cirlce: GMSCircle!
    
    //Mark : - For Use HOO View
    fileprivate var bagroundView = UIView()
    @IBOutlet weak var hooView: UIView!
    @IBOutlet weak var hooViewWinterHourLbl: UILabel!
    @IBOutlet weak var hooViewWinterTxtView: UITextView!
    
    //Mark : - For Use Ride Start POPup View
    fileprivate var ridebagroundView = UIView()
    @IBOutlet weak var ridePopupView: UIView!
    @IBOutlet weak var ridePopupTxtLbl: UILabel!
    @IBOutlet weak var ridePopupDoneBtn: UIButton!
    
    //Mark : - For Use Unlock View
    @IBOutlet weak var lockView: UIView!
    @IBOutlet weak var lockViewUnlockBtn: UIButton!
    @IBOutlet weak var activateLock: UIButton!
    @IBOutlet weak var lockViewTxtLabel: UILabel!
    @IBOutlet weak var lockViewTimeLabel: UILabel!
    @IBOutlet weak var lockViewTimeImage: UIImageView!
    @IBOutlet weak var lockViewBoardImg: UIImageView!
    var qrScanModelCopy : QRScanModel!
    
    //Mark : - For Use BLE Config
    var isBLEConnected : Bool = false
    var communicationManager : LinkaBLECommunicationManager!
    var isDeviceUnLocked : Bool = false
    
    //Mark : - For Use Resume Last ride
    var appCancelModel : AppCancelModel!
    
    let previewDemoData = [(title: "title is ", img: UIImage(named: "one_white_iPhone") ,price: 0), (title: "title is ", img: UIImage(named: "one_white_iPhone") ,price: 0), (title: "title is ", img: UIImage(named: "one_white_iPhone") ,price: 0), (title: "title is ", img: UIImage(named: "one_white_iPhone") ,price: 0)]
    let boardImageArray = [UIImage(named: "sup_board_iPhone"),UIImage(named: "perception-board_iPhone"),UIImage(named: "perception_rembler_iPhone"),UIImage(named: "glide_slup_iPhone")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // UserDefaults.standard.set(true, forKey: STRINGS.IS_BOARD_AACTIVE)
        CommonMethods.setStatusBarBackgroundColor(color: .black)
        apiCallForPlatform()
        
        //Open sidemenu on swipe
        let swipeToOpenMenu = UISwipeGestureRecognizer.init(target: self, action: #selector(openSideMenu))
        swipeToOpenMenu.delegate = self
        swipeToOpenMenu.direction = .right
        self.view.addGestureRecognizer(swipeToOpenMenu)
        isSideMenuOpen = false
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.activateLock.isHidden = true
        self.activateLock.layer.borderColor = UIColor.lightGray.cgColor
        self.activateLock.layer.borderWidth = 1.0
        checkBoardLock()
        CommonMethods.getLabelFontSizesWithDevices(label: titleLabel)
        sideMenuBagroundView.removeFromSuperview()
        inventoryView.removeFromSuperview()
        isSideMenuOpen = false
        self.isUpdateCurrentFirst = true
        if isTransactionSuccess(){
            //showAlertStartRide()
            //showRideStartPopup(UIButton())
            if isUnlockedSuccess() == false{
                showRideStartPopup(UIButton())
            }
        }
        if isDamageReportSubmitted(){
            ifDamageReportSubmit()
        }
        if  self.isBLEConnected == true && self.isDeviceUnLocked == true{
            self.pushToEndRideVc()
        }
        if  self.isBLEConnected == true && self.isDeviceUnLocked == false{
            self.apiCallForlock()
        }
    }
    
    func ifDamageReportSubmit(){
        self.lockViewUnlockBtn.setTitle("L O C K", for: .normal)
        self.lockViewUnlockBtn.tag = 1
        self.titleLabel.text = "L O C K  B O A R D/K A Y A K"
        self.availablityLabel.text = DynamicUserDataUtility.getInstance().screen_after_lock!
        self.lockViewBoardImg.transform = self.lockViewBoardImg.transform.rotated(by: .pi/2)
        self.activateLock.isHidden = false
        if let timed = UserDefaults.standard.value(forKey: STRINGS.TEXT_RIDETIME) as? String{
            self.lockViewTimeLabel.text = timed
        }
        dissmissRideStartPopup()
    }
    func dissmissRideStartPopup(){
        if self.ridePopupView != nil {
            SliderFunction.slideView(uiv_slide: self.ridePopupView, withDuration: 0.5, toX: 0, andY: self.view.frame.size.height, onCompletion: self.ridePopupView, toX: 0, andY: self.view.frame.size.height, button: UIButton())
            ridebagroundView.removeFromSuperview()
        }
    }
   func statusDelegate(_ status: Bool, communicationManager: LinkaBLECommunicationManager!, _ isDeviceLocked: Bool) {
        self.isBLEConnected = status
        self.communicationManager = communicationManager
        self.isDeviceUnLocked = isDeviceLocked
    }
    
    
    //Mark : - For Use Unlock View
    func checkBoardLock(){
        if let isUnlockedLocked = UserDefaults.standard.value(forKey: STRINGS.IS_BOARD_AACTIVE) as? Bool{
            if isUnlockedLocked{
                isUnlockedBoard()
            }else{
                isLockedBoard()
            }
        }else{
            isLockedBoard()
        }
    }
    func isUnlockedBoard(){
        self.paddleBtn.isHidden = true
        self.lockView.isHidden = false
        self.lockViewUnlockBtn.setTitle("U N L O C K", for: .normal)
        self.lockViewUnlockBtn.tag = 0
        self.titleLabel.text = "U N L O C K  Y O U R  B O A R D/K A Y A K"
        self.availablityLabel.text = String(format: "%d", DynamicUserDataUtility.getInstance().extra_min!) + " " + DynamicUserDataUtility.getInstance().extra_min_text!
        setQRScanData()
    }
    func isLockedBoard(){
        self.paddleBtn.isHidden = false
        self.lockView.isHidden = true
        self.availablityLabel.text = "AND CHECK AVAILABLITY"
        self.titleLabel.text = "S E L E C T  A  R A C K  L O C A T I O N"
    }
    func setQRScanData(){
        if let results = UserDefaults.standard.value(forKey: STRINGS.QRSCAN_MODEL_DATA) as? Data{
            do {
                self.qrScanModelCopy = try JSONDecoder().decode(QRScanModel.self, from: results)
                if qrScanModelCopy.status == "True"{
                    self.lockViewBoardImg.sd_setImage(with: URL(string: AppUrl.BASE_URL + self.qrScanModelCopy.image!.dropFirst()), placeholderImage: UIImage(named: "boat_iPhone"))
                    self.lockViewBoardImg.transform = self.lockViewBoardImg.transform.rotated(by: .pi/2)
                    self.lockViewTxtLabel.text = "\(self.qrScanModelCopy.board_Name!)\n $\(String(describing: self.qrScanModelCopy.before3hour! ))/min\n  & \n$\(String(describing: self.qrScanModelCopy.after3hour! ))/min after 3 hours"
                    self.hooViewWinterHourLbl.text = "Winter hours \(CommonMethods.convertTimeInAmPm(timeIs: self.qrScanModelCopy.start_booking_time!)) - \(CommonMethods.convertTimeInAmPm(timeIs: self.qrScanModelCopy.end_booking_time!))"
                    self.textViewConfig()
                }
            }catch (let error){
                print(error)
            }
        }
    }
    
    func textViewConfig(){
        let phoneNum = "7275954001"
        let url = URL(string: "tel://\(phoneNum)")
        
        let attributedString = NSMutableAttributedString(string: "All boards/kayaks must be returned by \(CommonMethods.convertTimeInAmPm(timeIs: self.qrScanModelCopy.board_submission_time!)) as the smart locks turn off at that time. \n State law does not allow paddling in low light conditions. There may be fees for any boards left unlocked (especially if ONE is not notified).\n Click here to call us immediately if issue arise.")
        let linkRange = (attributedString.string as NSString).range(of: "Click here")
        attributedString.addAttribute(NSAttributedString.Key.link, value: self.linkCallBtn() == true ? url! : "url", range: linkRange)
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(hexString: "0dc8f1"),
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.init(hexString: "0dc8f1"),
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        
        // textView is a UITextView
        self.hooViewWinterTxtView.linkTextAttributes = linkAttributes
        self.hooViewWinterTxtView.attributedText = attributedString
        self.hooViewWinterTxtView.textColor = UIColor.black
        self.hooViewWinterTxtView.textAlignment = .center
        self.hooViewWinterTxtView.delegate = self
        
    }
    func linkCallBtn() -> Bool{
        let phoneNum = "7275954001"
        if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
            return true
        }
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    @IBAction func activateLockButtonTapped(_ sender: UIButton) {
        // For Lock Case
        if self.isBLEConnected == false {
            self.isDeviceUnLocked = true
            self.moveToBLEConfigController()
        }
    }
    @IBAction func unlockButtonTapped(_ sender: UIButton) {
        if sender.tag == 0{
            //For Unlock Case
            self.hooView.frame = CGRect(x: 20, y: self.view.frame.size.height, width: self.view.frame.size.width-40, height: 190)
            SliderFunction.slideView(uiv_slide: self.hooView, withDuration: 0.5, toX: 20, andY: self.view.frame.size.height-380, onCompletion: self.hooView, toX: 20, andY: self.view.frame.size.height-380, button: sender)
            bagroundView.removeFromSuperview()
            bagroundView = UIView()
            bagroundView.frame = self.view.frame
            bagroundView.backgroundColor = UIColor.init(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.3)
            self.view.addSubview(bagroundView)
            self.view.addSubview(self.hooView)
        }else{
            // For Lock Case
            if self.isBLEConnected == false {
                self.isDeviceUnLocked = true
                self.moveToBLEConfigController()
            }
                //apiCallForlock()
        }
    }
    @IBAction func closeHooViewActnBtn(_ sender: UIButton) {
        if self.hooView != nil {
            SliderFunction.slideView(uiv_slide: self.hooView, withDuration: 0.5, toX: 10, andY: self.view.frame.size.height+10, onCompletion: self.hooView, toX: 10, andY: self.view.frame.size.height+10, button: sender)
            bagroundView.removeFromSuperview()
            showRideStartPopup(sender)
        }
    }
    @IBAction func rideStartPOpupDoneActnBtn(_ sender: UIButton) {
        if self.ridePopupView != nil {
            SliderFunction.slideView(uiv_slide: self.ridePopupView, withDuration: 0.5, toX: 0, andY: self.view.frame.size.height, onCompletion: self.ridePopupView, toX: 0, andY: self.view.frame.size.height, button: sender)
            ridebagroundView.removeFromSuperview()
            self.apiCallForUnlock()
        }
    }
    func showRideStartPopup(_ sender: UIButton){
        if self.ridePopupView != nil {
            self.ridePopupView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
            SliderFunction.slideView(uiv_slide: self.ridePopupView, withDuration: 0.5, toX: 0, andY: self.view.frame.size.height-self.view.frame.size.height, onCompletion: self.ridePopupView, toX: 0, andY: self.view.frame.size.height-self.view.frame.size.height, button: sender)
            ridebagroundView.removeFromSuperview()
            ridebagroundView = UIView()
            ridebagroundView.frame = self.view.frame
            ridebagroundView.backgroundColor = UIColor.init(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.3)
            self.view.addSubview(ridebagroundView)
            self.view.addSubview(self.ridePopupView)
            // Before Timer Start
            GlobalTimer.getInstance().seconds = 0
            //GlobalTimer.getInstance().startTimer()
        }
    }
    func pushToPayments(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let paymentViewControllerObj: PaymentsViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.PAYMENTS) as? PaymentsViewController
        CommonMethods.saveValueInUserDefaults(Value: "false", forKey: STRINGS.ISPAYMENTSKIP)
        self.navigationController?.pushViewController(paymentViewControllerObj!, animated: true)
    }
    //Mark : - check if transaction successfull
    func isUnlockedSuccess()->Bool{
        if let isUnlockedSuccess = UserDefaults.standard.value(forKey: STRINGS.ISFROMUNLOCK) as? String{
            if isUnlockedSuccess == "true"{
                return true
            }else{
                return false
            }
        }else{
            return true
        }
    }
    //Mark : - check if transaction successfull
    func isTransactionSuccess()->Bool{
        if let isTransactionSuccess = UserDefaults.standard.value(forKey: STRINGS.IS_TRANSACTION_SUCCESS) as? Bool{
            if isTransactionSuccess{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    //Mark : - check if Damage Report submitted
    func isDamageReportSubmitted()->Bool{
        if let isDamageReportSubmitted = UserDefaults.standard.value(forKey: STRINGS.IS_REPORT_SUBMITTED) as? Bool{
            if isDamageReportSubmitted{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    func configureViews(){
        setupViews()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        self.initGoogleMaps()
    }
    func setupViews(){
        availablityLabel.numberOfLines = 0
        //availablityLabel.insets = UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)
        inventoryView.frame =  CGRect(x: 20, y: 0, width: self.view.frame.width-60, height: 200)
    }
    
    func initGoogleMaps(){
        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 18.0)
        self.myView.camera = camera
        self.myView.delegate = self
        self.myView.isMyLocationEnabled = true
        
    }
    
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while getting location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //addCurrentLocationMarker()
        //locationManager.stopUpdatingLocation()
        let location = locations.last
        let lat = (location?.coordinate.latitude)!
        let long = (location?.coordinate.longitude)!
        //let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 18.0)
        // self.myView.animate(to: camera)
        if isUpdateCurrentFirst{
            showPartyMarkers()
        }else{
            addCurrentLocationMarker()
        }
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return false}
//        let img = customMarkerView.img!
        //        let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: img, borderColor: UIColor.white, tag: customMarkerView.tag)
        if self.platformDataArray.count > 0{
            marker.iconView = customMarkerView
            inventoryView.removeFromSuperview()
            inventoryView.frame =  CGRect(x: 20, y: myView.frame.origin.y + 10, width: self.view.frame.width-40, height: 200)
            inventoryView.prestentController = self
            self.view.addSubview(inventoryView)
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if gesture == true
        {
            inventoryView.removeFromSuperview()
        }
    }
    
    // MARK: Google Map Delegate
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return nil}
        if self.platformDataArray.count > 0{
            inventoryView.customMarkerTag = customMarkerView.tag
            inventoryView.platformDataArray = self.platformDataArray
            inventoryView.loadInventoryTableView()
            marker.infoWindowAnchor = CGPoint(x:0.45, y:0.20)
            return nil
        }
        return nil
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else{return}
        let tag = customMarkerView.tag
        for i in customMarkerView.subviews {
            if i.isKind(of: UITableView.self) {
                let table = i as! UITableView
                print(i.frame)
            }
        }
        //if info window tapped do stuff here
    }
    func addCurrentLocationMarker() {
        currentLocationMarker?.map = nil
        currentLocationMarker = nil
        if let location = locationManager.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18.0)
            if isUpdateCurrentFirst {
                self.myView.animate(to: camera)
                inventoryView.removeFromSuperview()
                self.isUpdateCurrentFirst = false
                if cirlce != nil{
                    cirlce.map = nil
                }
                cirlce = GMSCircle(position: camera.target, radius: 3000)
                cirlce.fillColor = UIColor(red: 0, green: 0, blue: 0.35, alpha: 0.1)
                cirlce.strokeColor = UIColor.blue
                cirlce.strokeWidth = 0
                cirlce.map = self.myView
            }
            
            currentLocationMarker = GMSMarker(position: location.coordinate)
            currentLocationMarker?.position = camera.target
            currentLocationMarker?.icon = UIImage(named: "location_iPhone")
            currentLocationMarker?.map = self.myView
            
        }
    }
    
    
    func showPartyMarkers(){
        myView.clear()
        addCurrentLocationMarker()
        for i in 0..<platformList.station_location!.count{
            let marker = GMSMarker()
            let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), borderColor: UIColor.darkGray, tag: i)
            marker.iconView = customMarker
            marker.position = CLLocationCoordinate2D(latitude: Double((platformList.station_location![i].latitude! as NSString).doubleValue), longitude: Double((platformList.station_location![i].longitude! as NSString).doubleValue))
            var bounds = GMSCoordinateBounds(location: marker.position, radiusMeters: 2000)
            marker.map = self.myView
            bounds = bounds.includingCoordinate(marker.position)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
            self.myView.animate(with: update)
            
            if platformList.success == "True"{
                apiCallForPlatformInfoWindow(id: platformList.station_location![i].id!)
            }else{
                CommonMethods.getSnackBarWithMessage(message: platformList.message!)
            }
        }
    }
    func btnMyLocation(){
        let location : CLLocation? = myView.myLocation
        if location != nil{
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 18.0)
            self.myView.animate(to: camera)
        }
    }
    @IBAction func paddleBtnActn(_ sender: UIButton){
        //location_iPhone
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let waiverViewControllerObj: WaiverViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.WAIVERVC) as? WaiverViewController
        waiverViewControllerObj?.urlStr = DynamicUserDataUtility.getInstance().agreement_file!
        self.navigationController!.pushViewController(waiverViewControllerObj!, animated: true)
    }
    
    @IBAction func menuBtnActn(_ sender: UIButton){
        if isSideMenuOpen == false {
            self.openSideMenu()
        }else{
            isSideMenuOpen = false
            SliderFunction.slideView(uiv_slide: sideMenuView, withDuration: 0.5, toX: 0-(self.view.frame.size.width-60), andY: 0, onCompletion: sideMenuView, toX: 0-(self.view.frame.size.width-100), andY: 0, button: menuBtn)
            sideMenuBagroundView.removeFromSuperview()
        }
    }
    
    //Side Menu Delegate
    func menuDelegate(_ bagroundView: UIView) {
        isSideMenuOpen = false
        sideMenuBagroundView.removeFromSuperview()
    }
    
    //Creates and open Side menu
    @objc func openSideMenu() {
        sideMenuView.removeFromSuperview()
        sideMenuView.frame = CGRect(x: 0-(self.view.frame.size.width-100), y: 0, width: (self.view.frame.size.width-60), height: self.view.frame.size.height)
        sideMenuBagroundView.removeFromSuperview()
        sideMenuBagroundView = UIView()
        sideMenuBagroundView.frame = self.view.frame
        sideMenuView.sideMenudelegate = self
        sideMenuBagroundView.backgroundColor = UIColor.init(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 0.3)
        sideMenuView.prestentController = self
        sideMenuView.loadSideMenu()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnBlurBG))
        tap.delegate = self
        
        let swipe = UISwipeGestureRecognizer.init(target: self, action: #selector(tapOnBlurBG))
        swipe.delegate = self
        swipe.direction = .left
        sideMenuView.addGestureRecognizer(swipe)
        self.view.addSubview(self.sideMenuBagroundView)
        self.view.addSubview(self.sideMenuView)
        
        SliderFunction.slideView(uiv_slide: sideMenuView, withDuration: 0.5, toX: 0, andY: 0, onCompletion: sideMenuView, toX: 0, andY: 0, button: menuBtn)
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.isSideMenuOpen = true
            self.sideMenuBagroundView.addGestureRecognizer(swipe)
            self.sideMenuBagroundView.addGestureRecognizer(tap)
        }
    }
    //Side menu extra blur baground tap action
    @objc func tapOnBlurBG() {
        sideMenuBagroundView.removeFromSuperview()
        isSideMenuOpen = false
        SliderFunction.slideView(uiv_slide: sideMenuView, withDuration: 0.5, toX: 0-(self.view.frame.size.width-60), andY: 0, onCompletion: sideMenuView, toX: 0-(self.view.frame.size.width-60), andY: 0, button: menuBtn)
    }
    // MARK: API Call for Platform
    func apiCallForPlatform(){
        self.callApiForResumeLastRide()
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: header, parameters: nil, apiName: AppApis.platformList, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.platformList = try JSONDecoder().decode(PlatformListModal.self, from: result!)
                    if (self.platformList.station_location?.count)! > 0 {
                        self.configureViews()
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    // MARK: API Call for PlatformListInfo
    func apiCallForPlatformInfoWindow(id: String){
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: id, requestType: RequestType.get, headers: header, parameters: nil, apiName: "", showLoading: false) { (false, result) in
            if let results = result{
                do {
                    self.platformInfoWindow = try JSONDecoder().decode(PlatformWindowModal.self, from: results)
                    if self.platformInfoWindow != nil{
                        self.platformDataArray.add(self.platformInfoWindow.data!)
                        print(self.platformDataArray.count)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    // MARK: API Call for Unlock Board
    func apiCallForUnlock(){
        if let linkaId = CommonMethods.getValueFromUserDefaults(key: STRINGS.LINKA_LOCK_ID) as? String{
            let param : Parameters = ["linka_lock_id" : linkaId]
            let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
            NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.unlock, showLoading: true) { (true, result) in
                if let results = result{
                    do {
                        let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
                        //CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        if loginResp.success == "True"{
                            //GlobalTimer.getInstance().stopTimer()
                            TIME.before_time = GlobalTimer.getInstance().seconds
                            UserDefaults.standard.set(TIME.before_time, forKey: STRINGS.TIME_BEFORE)
                            UserDefaults.standard.synchronize()
                            self.moveToBLEConfigController()
                            //self.pushToEndRideVc()
                        }else{
                            if loginResp.message! == "Board already in use "{
                                return
                            }else{
                                CommonMethods.saveValueInUserDefaults(Value: "true", forKey: STRINGS.ISFROMUNLOCK)
                                self.pushToPayments()
                            }
                        }
                    }catch (let error){
                        print(error)
                    }
                }else{
                    GlobalTimer.getInstance().stopTimer()
                }
            }
        }
    }
    // MARK: API Call for lock Board
    func apiCallForlock(){
        if let beforeTime = UserDefaults.standard.value(forKey: STRINGS.TIME_BEFORE) as? Int , let afterTime = UserDefaults.standard.value(forKey: STRINGS.TIME_AFTER) as? Int , let rideTime = UserDefaults.standard.value(forKey: STRINGS.TIME_RIDETIME) as? Int{
            let param : Parameters = ["extra_time_taken_after_ending_a_ride" : (beforeTime + afterTime), "ride_Time": rideTime]
            let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
            NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.lock, showLoading: true) { (true, result) in
                if let results = result{
                    do {
                        let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        if loginResp.success == "True"{
                            UserDefaults.standard.set(false, forKey: STRINGS.IS_BOARD_AACTIVE)
                            UserDefaults.standard.set(false, forKey: STRINGS.IS_REPORT_SUBMITTED)
                            UserDefaults.standard.set(false, forKey: STRINGS.IS_TRANSACTION_SUCCESS)
                            UserDefaults.standard.removeObject(forKey: STRINGS.TEXT_RIDETIME)
                            self.lockViewTimeLabel.text = GlobalTimer.getInstance().timeString(time: TimeInterval(0))
                            UserDefaults.standard.set(self.lockViewTimeLabel.text, forKey: STRINGS.TEXT_RIDETIME)
                            UserDefaults.standard.synchronize()
                            self.pushToFeedbackVc()
                            self.isBLEConnected = false
                        }else{
                            print(loginResp.success)
                        }
                    }catch (let error){
                        print(error)
                    }
                }
            }
        }
    }
    
    //MARK:- Api call for Resume Last Ride
    func callApiForResumeLastRide(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.appCancel , showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.appCancelModel = try JSONDecoder().decode(AppCancelModel.self, from: results)
                    if self.appCancelModel.success == "False"{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "EndRideViewController") as! EndRideViewController
                        vc.appCancelModel = self.appCancelModel
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    func pushToFeedbackVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func pushToEndRideVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EndRideViewController") as! EndRideViewController
        vc.qrScanModelCop = self.qrScanModelCopy
        self.isBLEConnected =  false
//        self.isDeviceUnLocked = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //************************* For Lock/Unlock Use ***********************//
    func moveToBLEConfigController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AvailableBLELocksViewController") as! AvailableBLELocksViewController
        vc.bleStatusDelegate = self
        vc.isDeviceUnLocked = self.isDeviceUnLocked
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension GMSCoordinateBounds {
    convenience init(location: CLLocationCoordinate2D, radiusMeters: CLLocationDistance) {
        let region = MKCoordinateRegion(center: location, latitudinalMeters: radiusMeters, longitudinalMeters: radiusMeters)
        self.init(coordinate: region.northWest, coordinate: region.southEast)
    }
}
extension MKCoordinateRegion {
    var northWest: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude + span.latitudeDelta / 2, longitude: center.longitude - span.longitudeDelta / 2)
    }
    
    var southEast: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude - span.latitudeDelta / 2, longitude: center.longitude + span.longitudeDelta / 2)
    }
}
