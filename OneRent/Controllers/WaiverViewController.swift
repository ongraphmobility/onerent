//
//  WaiverViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 03/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class WaiverViewController: UIViewController,UIWebViewDelegate,UIScrollViewDelegate {


    @IBOutlet weak var webViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var waiverWebView: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var waiverTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var waiverBottomView: UIView!
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var scrollDownAgreeBtn: UIButton!
    var urlStr = String()
    var isAllTrue = false
    var aggrementDict = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DEFINED_ARRAYS.waiverTitleArr.removeAllObjects()
        DEFINED_ARRAYS.waiverTitleArr = [["isAdded":false, "title":"I ACKNOWLEDGE THAT LIFE JACKETS ARE REQUIRED AT ALL TIMES"],["isAdded":false, "title":"I CERTIFY THAT I AM 18 YEARS OF AGE OR OLDER"],["isAdded":false, "title":"I CERTIFY THAT I AM ABLE TO SWIM"],["isAdded":false, "title":"IF THUNDERSTORMS ARE IN THE AREA THEN IT IS MY RESPONSIBILITY TO WAIT UNTIL THEY’VE PASSED BEFORE I ENTER THE WATER. OR, IF I’M IN THE WATER, THEN I WILL IMMEDIATELY EXIT"],["isAdded":false, "title":"I’VE READ THE METAL PLACARD LOCATED IN FRONT OF THE ONE RACK AND I AGREE TO FOLLOW THE LOCAL LAWS"],["isAdded":false, "title":"I ACKNOWLEDGE THAT I’VE CHECKED THE WIND/WEATHER/CURRENT CONDITIONS ON THE ‘SAFETY & CONDITIONS’ TAB AND FEEL CONFIDENT I HAVE THE SKILLS NECESSARY TO TRAVEL SAFELY ON THIS WATERWAY."],["isAdded":false, "title":"I AGREE THAT IF WINDS ARE OVER 15 MPH (13 KNTS) OR CURRENTS ARE TOO STRONG, I WILL RETURN THE BOARD/KAYAK TO THE RACK IMMEDIATELY."],["isAdded":false, "title":"I ACKNOWLEDGE THAT I AM RIDING AT MY OWN RISK"],["isAdded":false, "title":"I AGREE THAT BY TYPING MY NAME AS MY SIGNATURE ON THIS FORM BELOW, I AM WAIVING ANY AND ALL RIGHTS I HAVE TO HOLD ONE BOARD RENTALS, LLC. LIABLE FOR ANY REASON."]]
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        loadWaiverWebView()
    }
    func loadWaiverWebView(){
        //Document file url
        let docUrl = NSURL(string: AppUrl.BASE_URL + urlStr.dropFirst())
        let req = NSURLRequest.init(url: docUrl! as URL)
        waiverWebView.delegate = self
        //here is the sole part
        waiverWebView.scalesPageToFit = true
        waiverWebView.scrollView.showsVerticalScrollIndicator = false
        waiverWebView.contentMode = .scaleAspectFit
        waiverWebView.loadRequest(req as URLRequest)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let contentSize = webView.scrollView.contentSize
        print(contentSize.height)
        webViewHeightConstant.constant = contentSize.height
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: contentSize.height)
        scrollView.delegate = self
        bottomViewHeightConstant.constant = bottomViewHeightConstant.constant + 330
        waiverTableView.estimatedRowHeight = 100
        waiverTableView.rowHeight = UITableView.automaticDimension
        waiverTableView.sectionHeaderHeight = UITableView.automaticDimension
        waiverTableView.estimatedSectionHeaderHeight = 64
        waiverTableView.separatorStyle = .none
        waiverTableView.delegate = self
        waiverTableView.dataSource = self
        waiverTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CommonMethods.getLabelFontSizesWithDevices(label: titleLabel)
    }

    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func scrollDownBtnActn(_ sender : UIButton){
        scrollView.scrollToBottom()
        scrollDownAgreeBtn.isHidden = true
    }
    @IBAction func agreeBtnActn(_ sender : UIButton){
        if nameTxtField.text?.isEmpty == true{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter your name", sender: self)
            return
        }
        if isAllTrue == false{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please accept rider agreement", sender: self)
            return
        }
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let waiverEmailViewControllerObj: WaiverEmailedViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.WAIVEREMAILVC) as? WaiverEmailedViewController
        waiverEmailViewControllerObj?.enteredName = nameTxtField.text!
        self.navigationController?.pushViewController(waiverEmailViewControllerObj!, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            scrollDownAgreeBtn.isHidden = true
        }
        if (scrollView.contentOffset.y <= 0){
            //reach top
            scrollDownAgreeBtn.isHidden = false
        }
        if (scrollView.contentOffset.y > 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
            //not top and not bottom
            scrollDownAgreeBtn.isHidden = true
        }
    }
    
    
    
}
extension WaiverViewController : UITableViewDelegate, UITableViewDataSource{
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DEFINED_ARRAYS.waiverTitleArr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "WaiverHeaderTableCell") as! WaiverHeaderTableCell
        headerView.addSubview(headerCell)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WaiverTableCell", for: indexPath) as! WaiverTableCell
        cell.selectionStyle = .none
        let dict = DEFINED_ARRAYS.waiverTitleArr.object(at: indexPath.row) as? [String:Any]
        cell.lblTitle?.text = dict!["title"] as? String
        if dict!["isAdded"] as! Bool{
            cell.checkUncheckImage.image = UIImage.init(named: "checkbox_blue_iPhone")
        }else{
            cell.checkUncheckImage.image = UIImage.init(named: "checkbox_white_iPhone")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = DEFINED_ARRAYS.waiverTitleArr.object(at: indexPath.row) as? [String:Any]
        if dict!["isAdded"] as! Bool{
            dict!["isAdded"] = false
        }else{
            dict!["isAdded"] = true
        }
        DEFINED_ARRAYS.waiverTitleArr.replaceObject(at: indexPath.row, with: dict!)
         isAllTrue = false
        for i in 0..<DEFINED_ARRAYS.waiverTitleArr.count{
           let dict = DEFINED_ARRAYS.waiverTitleArr.object(at: i) as? [String:Any]
            if dict!["isAdded"] as! Bool == true{
                isAllTrue = true
            }else{
                isAllTrue = false
                break
            }
        }
        if isAllTrue == false{
            agreeBtn.backgroundColor = UIColor.darkGray
        }else{
            agreeBtn.backgroundColor = UIColor(red: 0, green: 210, blue: 240, alpha: 1)
        }
        self.waiverTableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
    }
}
extension UIScrollView {
    
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
}
