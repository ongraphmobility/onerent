//
//  EndRideViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 19/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class EndRideViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var myMapView: GMSMapView!
    var currentLocationMarker: GMSMarker?
    var isUpdateCurrentFirst : Bool = true
    var locationManager = CLLocationManager()
    
    //Mark : - For Use End Ride View
    @IBOutlet weak var lockView: UIView!
    @IBOutlet weak var lockViewpaddleEndRideBtn: UIButton!
    @IBOutlet weak var lockViewTxtLabel: UILabel!
    @IBOutlet weak var lockViewTimeLabel: UILabel!
    @IBOutlet weak var lockViewBoardImg: UIImageView!
    var seconds = 00
    var isTimerRunning = false
    weak var timer: Timer?
    var qrScanModelCop : QRScanModel!
    
    //Mark : - For Use Resume Last ride
    var appCancelModel : AppCancelModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.appCancelModel != nil{
            if self.appCancelModel.success == "False"{
                setLastRideData()
                configureLocation()
                print((self.appCancelModel.canceldata?.start_ride_time)!)
                let secondds = self.convertDateFormatter(date: (self.appCancelModel.canceldata?.start_ride_time)!)
                startTimer()
            }
        }else{
            setData()
            configureLocation()
            startTimer()
        }
        
    }
    func setLastRideData(){
        self.lockViewBoardImg.sd_setImage(with: URL(string: AppUrl.BASE_URL + self.appCancelModel.canceldata!.image!.dropFirst()), placeholderImage: UIImage(named: "boat_iPhone"))
        self.lockViewBoardImg.transform = self.lockViewBoardImg.transform.rotated(by: .pi/2)
        self.lockViewTxtLabel.text = "\(self.appCancelModel.canceldata!.board_Name!)\n $\(String(describing: self.appCancelModel.canceldata!.before3hour! ))/min\n & \n$\(String(describing: self.appCancelModel.canceldata!.after3hour! ))/min after 3 hours"
    }
    func setData(){
        self.lockViewBoardImg.sd_setImage(with: URL(string: AppUrl.BASE_URL + self.qrScanModelCop.image!.dropFirst()), placeholderImage: UIImage(named: "boat_iPhone"))
        self.lockViewBoardImg.transform = self.lockViewBoardImg.transform.rotated(by: .pi/2)
        self.lockViewTxtLabel.text = "\(self.qrScanModelCop.board_Name!)\n $\(String(describing: self.qrScanModelCop.before3hour! ))/min\n & \n$\(String(describing: self.qrScanModelCop.after3hour! ))/min after 3 hours"
    }
    func configureLocation(){
        initializeTheLocationManager()
        self.myMapView.delegate = self
        self.myMapView.isMyLocationEnabled = true
        myMapView.settings.compassButton = true
        myMapView.settings.myLocationButton = true
        
       // addCurrentLocationMarker()
    }
    func initializeTheLocationManager() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    

    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locationManager.location?.coordinate
        cameraMoveToLocation(toLocation: location)
    }
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            self.myMapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 15)
        }
    }
    func startTimer() {
        if isTimerRunning == false {
            runTimer()
        }
    }
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(EndRideViewController.updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    func stopTimer(){
        timer?.invalidate()
        lockViewTimeLabel.text = timeString(time: TimeInterval(seconds))
        TIME.totalRideTime = seconds
        UserDefaults.standard.set(lockViewTimeLabel.text, forKey: STRINGS.TEXT_RIDETIME)
        UserDefaults.standard.set(TIME.totalRideTime, forKey: STRINGS.TIME_RIDETIME)
        UserDefaults.standard.synchronize()
        isTimerRunning = false
        UserDefaults.standard.set(false, forKey: STRINGS.IS_TRANSACTION_SUCCESS)
        UserDefaults.standard.synchronize()
        pushToReportVc()
    }
    @objc func updateTimer() {
            seconds += 1
            lockViewTimeLabel.text = timeString(time: TimeInterval(seconds))
    }
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    func pushToReportVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReportDamageViewController") as! ReportDamageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func onOkayEndRide(){
        stopTimer()
    }
    @objc func onCancelEndRide(){
        print("Cancel Called")
    }
    @IBAction func backBtnActn(_ sender : UIButton){
        if isTimerRunning{
            CommonMethods.showAlert("One Rent", andMessage: "Are you sure to end this Ride?", andCallMethodOnOk: #selector(self.onOkayEndRide), andCallMethodOnCancel: #selector(self.onCancelEndRide), fromObject: self, on: self)
        }else{
           self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func endRideBtnActn(_ sender : UIButton){
        stopTimer()
    }
    func convertDateFormatter(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")//TimeZone(name: "UTC")
        let date = dateFormatter.date(from: date)
        let compo = CommonMethods.dateDifferenceformatter(date: date!)
        print(compo.second)
        seconds = compo.second!
        dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"///this is what you want to convert format
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
    }
}
