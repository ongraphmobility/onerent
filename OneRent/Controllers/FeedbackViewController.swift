//
//  FeedbackViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 23/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rideLabel: UILabel!
    @IBOutlet weak var helpUsLabel: UILabel!
    @IBOutlet weak var rideTimeLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var feedbackTxtView: UITextView!
    @IBOutlet weak var oneStarBtn: UIButton!
    @IBOutlet weak var twoStarBtn: UIButton!
    @IBOutlet weak var threeStarBtn: UIButton!
    @IBOutlet weak var fourStarBtn: UIButton!
    @IBOutlet weak var fiveStarBtn: UIButton!
    
    
    var ratingNum : Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackTxtView.delegate = self
        self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
        self.twoStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
        self.threeStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
        self.fourStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
        self.fiveStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
        self.ratingNum = 5
    }
    //MARK:- UITextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        print(textView.text)
        if textView.text == "Enter Feedback..."{
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text?.isEmpty)!{
            textView.text = "Enter Feedback..."
        }
    }

    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
     @IBAction func submitBtnActn(_ sender : UIButton){
        if feedbackTxtView.text.isEmpty == true{
            CommonMethods.getSnackBarWithMessage(message: "Please write something about ride.. ")
            return
        }
        if self.ratingNum == 0{
            CommonMethods.getSnackBarWithMessage(message: "Please rate us..")
            return
        }
        apiCallForPostFeedbackData()
    }
    
    @IBAction func starBtnActn(_ sender : UIButton){
        switch sender.tag {
        case 1:
            self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.twoStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.threeStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.fourStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.fiveStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.ratingNum = sender.tag
        case 2:
            self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.twoStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.threeStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.fourStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.fiveStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.ratingNum = sender.tag
        case 3:
            self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.twoStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.threeStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.fourStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.fiveStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.ratingNum = sender.tag
        case 4:
            self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.twoStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.threeStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.fourStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.fiveStarBtn.setImage(UIImage.init(named: "star_gray_iPhone"), for: .normal)
            self.ratingNum = sender.tag
        default:
            self.oneStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.twoStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.threeStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.fourStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.fiveStarBtn.setImage(UIImage.init(named: "star_blue_iPhone"), for: .normal)
            self.ratingNum = sender.tag
        }
    }
    
    // MARK: API Call for Post FeedBack
    func apiCallForPostFeedbackData(){
        let param : Parameters = ["rating": self.ratingNum!, "review_content": feedbackTxtView.text!]
            let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
            NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.feedback, showLoading: true) { (true, result) in
                if let results = result{
                    do {
                         let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
                        if loginResp.success == "True"{
                            self.pushToPaddleRecapVc()
                        }
                    }catch (let error){
                        print(error)
                    }
                }
            }
    }
   
    func pushToPaddleRecapVc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PaddleRecapViewController") as! PaddleRecapViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
