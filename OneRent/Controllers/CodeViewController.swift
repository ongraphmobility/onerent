//
//  CodeViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit
import Alamofire

class CodeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtCode : UITextField!
    @IBOutlet weak var btnPaddle : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // CommonMethods.setStatusBarBackgroundColor(color: .black)
        txtCode.delegate = self
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "ENTER CODE"{
            textField.text = ""
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if (txtCode.text?.isEmpty)!{
            textField.text = "ENTER MAIL"
        }
        return true
    }
    
    @IBAction func backBtnActn(_ sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func paddleBtnActn(_ sender : UIButton){
        if txtCode.text?.isEmpty == false{
            self.view.endEditing(true)
            callApiVerifyLoginCode(code: txtCode.text!)
        }else{
            CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_CODE, sender: self)
        }
        
    }

    func callApiVerifyLoginCode(code:String){
        
        let param: Parameters = ["code": code]
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.login + AppApis.code, showLoading: true) { (true, result) in
            
            if let isResult = result{
            do {
                let loginResp = try JSONDecoder().decode(LoginResponse.self, from: result!)
                
                if loginResp.success == "true" {
                    if  loginResp.is_new_user == "false"{
                        CommonMethods.saveValueInUserDefaults(Value: loginResp.token!, forKey: STRINGS.TOKEN_KEY)
                        CommonMethods.saveValueInUserDefaults(Value: STRINGS.LOGGEDIN_USER, forKey: "UserTyp")
                        CommonMethods.saveValueInUserDefaults(Value: (loginResp.user?.email ?? ""), forKey: STRINGS.USER_EMAIL)
                        UserDefaults.standard.set(loginResp.user?.verified, forKey: STRINGS.IS_USER_VERIFIED)
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(loginResp.user?.id, forKey: STRINGS.USER_ID)
                        UserDefaults.standard.synchronize()
                        self.pushToHome()
                    }else{
                        
                        CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_LOGIN_CODE, sender: self)
                    }
                    
                }
            }catch (let error){
                print(error)
            }
            }
        }
    }
    
    func pushToHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigatioStack = UINavigationController(rootViewController: vc)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }

}
