//
//  SafetyTrainingViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 14/02/19.
//  Copyright © 2019 Ongraph Technology. All rights reserved.
//

import UIKit

class SafetyTrainingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    func launchApp(decodedURL: String) {
        if let url = URL(string: decodedURL) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else{
            CommonMethods.getSnackBarWithMessage(message: "Oop's something went wrong.")
        }
    }
    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func safetyBtnActn(_ sender : UIButton){
        let url = "https://www.youtube.com/channel/UCFtWtICJUNV4Qk5sa6UL5Wg"
        launchApp(decodedURL: url)
    }
    @IBAction func trainingBtnActn(_ sender : UIButton){
        let urlweb = "https://oneboardrentals.com"
        launchApp(decodedURL: urlweb)
    }
}
