//
//  ViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 24/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtZipCode : UITextField!
    @IBOutlet weak var zipCodeBottomView : UIView!
    @IBOutlet weak var txtView : UITextView!
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var btnPaddle : UIButton!
    @IBOutlet weak var topTaglineLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       topTaglineLabel.text = DynamicUserDataUtility.getInstance().agreement_age_text
       txtZipCode.isHidden = true
       zipCodeBottomView.isHidden = true
       txtView.isHidden = true
       lblNote.isHidden = true
       txtEmail.delegate = self
       txtZipCode.delegate = self
       txtEmail.autocorrectionType = .no
        textView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
       // CommonMethods.setStatusBarBackgroundColor(color: .black)
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if DynamicUserDataUtility.getInstance().zipcode_required_field == true{
            txtZipCode.isHidden = false
            zipCodeBottomView.isHidden = false
            if textField.text == "ENTER ZIPCODE"{
                textField.text = ""
            }
        }
        if textField.text == "ENTER YOUR EMAIL"{
            textField.text = ""
            txtView.isHidden = false
            lblNote.isHidden = false
            moveViewOnTF(moveValue: -250)
            }
        
        }
        func moveViewOnTF(moveValue:CGFloat){
            UIView.animate(withDuration: 0.2) {
                self.view.frame.origin.y = moveValue
            }
        }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
        if (txtEmail.text?.isEmpty)!{
            textField.text = "ENTER YOUR EMAIL"
        }
        
        if (txtZipCode.text?.isEmpty)!{
            textField.text = "ENTER ZIPCODE"
        }
        
    }
    
    @IBAction func btnPaddleTapped(_ sender : UIButton){
        if txtEmail.text != "ENTER YOUR EMAIL" || txtEmail.text?.isEmpty == false {
            if CommonMethods.isValidEmail(testStr: txtEmail.text!) == false {
                CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_EMAIL_ERROR, sender: self)
                return
            }
        }
        if txtZipCode.isHidden == false{
            if txtZipCode.text == "ENTER ZIPCODE" || txtZipCode.text?.isEmpty == true  {
                CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: ERRORS.INVALID_ZIPCODE_ERROR, sender: self)
                return
            }
            if (txtZipCode.text?.count)! < 5 || (txtZipCode.text?.count)! > 5{
                CommonMethods.showAlert(withTitle: STRINGS.ALERT, alertMessage: "Please enter valid zipcode", sender: self)
                return
            }
        }else{
            txtZipCode.text = ""
        }
        self.view.endEditing(true)
        callApiForLogin()
    }
    func popupEnterCreditCard(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyBoard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        popupVC.modalPresentationStyle = .popover
        popupVC.preferredContentSize = CGSize(width:300, height: 300)
        let pVC = popupVC.popoverPresentationController
        pVC?.permittedArrowDirections = .any
        pVC?.delegate = self
        pVC?.sourceView = self.view
        pVC?.sourceRect = CGRect(x: 100, y: 100, width: 1, height: 1)
        CommonMethods.saveValueInUserDefaults(Value: "true", forKey: STRINGS.ISPAYMENTSKIP)
        self.navigationController?.pushViewController(popupVC, animated: true)
    }
    func textView(){
        let attributedString = NSMutableAttributedString(string: "By clicking 'Paddle', I agree to One Board Rental's Terms of Service and Privacy Policy.")
        let linkRange1 = (attributedString.string as NSString).range(of: "Terms of Service")
        attributedString.addAttribute(NSAttributedString.Key.link, value: AppUrlKey.termsOfService, range: linkRange1)
        let linkRange2 = (attributedString.string as NSString).range(of: "Privacy Policy")
        attributedString.addAttribute(NSAttributedString.Key.link, value: AppUrlKey.privacyPolicy, range: linkRange2)
        
       let linkAttributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white,
        NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
        NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        
        // textView is a UITextView
        txtView.linkTextAttributes = linkAttributes
        txtView.attributedText = attributedString
        txtView.textColor = UIColor.lightGray
        txtView.textAlignment = .center
        txtView.delegate = self
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    
       func callApiForLogin(){
       
        let param: Parameters = ["username": txtEmail.text!, "zipcode": txtZipCode.isHidden==false ? Int(txtZipCode.text!) as Any : "" ]
        let header : HTTPHeaders = CommonMethods.header()
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.login, showLoading: true) { (true, result) in
            
        if let results = result{
            do {
                let loginResp = try JSONDecoder().decode(LoginResponse.self, from: results)
               
                if loginResp.success == "true" {
                    if loginResp.message == "Login is successfull" && loginResp.is_new_user == "true"{
                        //self.pushToVerifyUser()
                        CommonMethods.saveValueInUserDefaults(Value: loginResp.token!, forKey: STRINGS.TOKEN_KEY)
                        CommonMethods.saveValueInUserDefaults(Value: STRINGS.LOGGEDIN_USER, forKey: "UserTyp")
                        CommonMethods.saveValueInUserDefaults(Value: (loginResp.user?.email ?? ""), forKey: STRINGS.USER_EMAIL)
                        UserDefaults.standard.set(loginResp.user?.verified, forKey: STRINGS.IS_USER_VERIFIED)
                        UserDefaults.standard.synchronize()
                        UserDefaults.standard.set(loginResp.user?.id, forKey: STRINGS.USER_ID)
                        UserDefaults.standard.synchronize()
                        self.popupEnterCreditCard()
                    }else{
                        self.pushToVerifyUser()
                    }
                
                }
            }catch (let error){
                print(error)
            }
         }
        }
    }
    func pushToVerifyUser(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
        vc.emailString = txtEmail.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}


