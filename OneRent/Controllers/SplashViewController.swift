//
//  SplashViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var taglineLabel:UILabel!
    var dynamicModel : [DynamicModel]!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let dynamicResultData = UserDefaults.standard.object(forKey: STRINGS.USER_DYNAMIC_DATA) as? Data else {return}
        do {
            dynamicModel = try JSONDecoder().decode([DynamicModel].self, from: dynamicResultData)
            DynamicUserDataUtility.getInstance().setValues(data: dynamicModel)
            taglineLabel.text = DynamicUserDataUtility.getInstance().main_logo_screen_tagline
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
                self.moveToNextStep()
            })
        }catch (let error){
            print(error)
        }
    }
    
    func moveToNextStep(){
        ///To check user is comming first time or not
        if UserDefaults.standard.object(forKey: STRINGS.USER_TYP_KEY) == nil{
            worksTutorial()
        }else{
            print(UserDefaults.standard.object(forKey: STRINGS.IS_USER_VERIFIED) as Any)
            if UserDefaults.standard.object(forKey: STRINGS.IS_USER_VERIFIED) as! Bool == true{
                pushToHome()
            }else{
                moveUserToLogin()
            }
            
        }
    }
    
    func worksTutorial(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let worksViewController = storyBoard.instantiateViewController(withIdentifier: "HowWorksViewController") as! HowWorksViewController
        worksViewController.isfromSplash = true
        let navigatioStack = UINavigationController(rootViewController: worksViewController)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }
    func moveUserToLogin(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navigatioStack = UINavigationController(rootViewController: viewController)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }
    func pushToHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigatioStack = UINavigationController(rootViewController: vc)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }

   

}
