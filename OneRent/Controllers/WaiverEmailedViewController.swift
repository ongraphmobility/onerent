//
//  WaiverEmailedViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 10/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class WaiverEmailedViewController: UIViewController {
    @IBOutlet weak var addAditionalBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    var enteredName = String()
    var agreementArray = NSMutableArray()
    var dict = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        if let email = CommonMethods.getValueFromUserDefaults(key: STRINGS.USER_EMAIL) as? String {
            dict.setValue(email, forKey: "email")
            dict.setValue(enteredName, forKey: "name")
            agreementArray.add(dict)
        }
    }
    func setupViews(){
        addAditionalBtn.layer.cornerRadius = 5.0
        addAditionalBtn.layer.masksToBounds = true
        addAditionalBtn.layer.borderColor = UIColor.black.cgColor
        addAditionalBtn.layer.borderWidth = 1.0
        doneBtn.layer.cornerRadius = 5.0
        doneBtn.layer.masksToBounds = true
        doneBtn.layer.borderColor = UIColor.black.cgColor
        doneBtn.layer.borderWidth = 1.0
    }
    @IBAction func additionalBtnActn(_ sender : UIButton){
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "AlertVIewController") as! AlertVIewController
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
       }
    @IBAction func doneBtnActn(_ sender : UIButton){
        apiCallForagreement()
    }
    // MARK: API Call for Agreement
    func apiCallForagreement(){
        let header : HTTPHeaders = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        let param: Parameters = ["agreement": agreementArray]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: header, parameters: param, apiName: AppApis.agreement, showLoading: true) { (false, result) in
            if let results = result{
                print(results)
                CommonMethods.getSnackBarWithMessage(message: "Thank you For Signing The Agreement")
                
                if #available(iOS 10.2, *) {
                    self.pushToScanController()
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    @available(iOS 10.2, *)
    func pushToScanController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: CONTROLLERS.SCANQRVC) as! ScanQRViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension WaiverEmailedViewController: CustomAlertViewDelegate {
    
    func okButtonTapped(nameTextFieldValue: String, emailTextFieldValue: String) {
        print(nameTextFieldValue)
        print(emailTextFieldValue)
        dict.setValue(emailTextFieldValue, forKey: "email")
        dict.setValue(nameTextFieldValue, forKey: "name")
        agreementArray.add(dict)
    }
    
    func cancelButtonTapped() {
        print("cancelButtonTapped")
    }
}
