//
//  RideHistoryViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 23/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class RideHistoryViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var historyTableView: UITableView!
    var historyModel : RideHistoryModel!
    var historyDataArray : [HistoryData]!
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTableView.estimatedRowHeight = 80
        historyTableView.rowHeight = UITableView.automaticDimension
        apiCallForHistoryData()
    }
    func formatter(dateString : String) -> String?{
        let dateFormatter = DateFormatter()
        let strformatter = DateFormatter()
        strformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")//TimeZone(name: "UTC")
        let date = dateFormatter.date(from: dateString)
        if date == nil{
            return ""
        }
        let myString = strformatter.string(from: date!)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let myStringafd = dateFormatter.string(from: date!)
        print(myStringafd)
        let dat = myString.components(separatedBy: " ")
        let time = CommonMethods.convertTimeInAmPm(timeIs: dat[1])
        print(time)
        return String(format: "%@, %@",myStringafd,time)
    }
    
    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: API Call for Get History
    func apiCallForHistoryData(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.history, showLoading: true) { (true, result) in
            if let results = result{
                do {
                    self.historyModel = try JSONDecoder().decode(RideHistoryModel.self, from: results)
                    if self.historyModel.success == "True"{
                        self.historyDataArray = self.historyModel.data
                        if self.historyDataArray.count > 0{
                            self.historyTableView.delegate = self
                            self.historyTableView.dataSource = self
                            self.historyTableView.reloadData()
                        }
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}

extension RideHistoryViewController : UITableViewDelegate, UITableViewDataSource{
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        cell.selectionStyle = .none
        cell.dateTimeLabel.text = self.formatter(dateString: self.historyDataArray[indexPath.row].start_ride_time!)
        cell.rideTimeLabel.text = self.historyDataArray[indexPath.row].ride_Time != nil ? String(format: "%@", self.timeString(time: self.historyDataArray[indexPath.row].ride_Time!)) : "0 hr : 0 min : 0 sec"
        cell.rideAmountLabel.text =  self.historyDataArray[indexPath.row].ride_time_amount != nil ? String(format: "$ %@", self.historyDataArray[indexPath.row].ride_time_amount!) : "$ 0.00"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func timeString(time:String) -> String {
        let timr = time.components(separatedBy: ":")
        return String(format:"%@ hr %@ min %@ sec", timr[0], timr[1], timr[2])
    }
}
