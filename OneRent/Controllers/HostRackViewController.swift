//
//  HostRackViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class HostRackViewController: UIViewController {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var headingLabel:UILabel!
    @IBOutlet weak var subheadingOneLabel:UILabel!
    @IBOutlet weak var subheadingTwoLabel:UILabel!
    @IBOutlet weak var getStartedBtn:UIButton!
    var hostRackModel : HostRackModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCallForGetHostRackData()
    }
    //MARK:- IBActions methods
    @IBAction func backBtnActn(_ sender : UIButton){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                
                tasks.forEach({$0.cancel()})
            }
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getStartedBtnActn(_ sender : UIButton){
        apiCallForPostHostRackData()
    }
    
    // MARK: API Call for Get Host rack Data
    func apiCallForGetHostRackData(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.get, headers: headerParams, parameters: nil, apiName: AppApis.hostRack, showLoading: true) { (true, result) in
            if let results = result{
                do {
                   self.hostRackModel = try JSONDecoder().decode(HostRackModel.self, from: results)
                    if self.hostRackModel.status == "True"{
                        self.subheadingOneLabel.text = self.hostRackModel.data?.host_a_rack_text!
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    // MARK: API Call for Post Host rack Data
    func apiCallForPostHostRackData(){
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: nil, apiName: AppApis.hostRack, showLoading: true) { (true, result) in
            if let results = result{
                CommonMethods.showAlert(withTitle: "One Board Rentals", alertMessage: "Please answer the list of questions that have been emailed to you.", sender: self)
            }
        }
    }
}
