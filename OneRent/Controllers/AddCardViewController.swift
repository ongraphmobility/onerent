//
//  AddCardViewController.swift
//  OneRent
//
//  Created by Ambuj Singh on 12/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit
import Stripe

class AddCardViewController: UIViewController, STPPaymentCardTextFieldDelegate {

    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var saveChangesBtn: UIButton!
    @IBOutlet weak var deleteCardBtn: UIButton!
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    var dictPayData = NSMutableDictionary()
    let paymentTextField = STPPaymentCardTextField()
    var cardTest1: iCreditCard?
    var expYear : String?
    var expMonth : String?
    var cardHolderName : String?
    var userCCInfoArray : [UserCCData]!
    
    
    fileprivate func test2(){
         cardTest1 = iCreditCard(cardStyleWithBackgroundImage: .withBottomBankIcon, withCardImage: UIImage(named:"card_color_iPhone"), pinIcon: .chip2, creditIcons: .maestroIcon)
        cardTest1?.topSeparator.isHidden = true
        view.addSubview(cardTest1!)

        cardTest1?.snp.makeConstraints({ (make) in
            make.height.equalTo(220)
            make.width.equalTo(320)
            make.top.equalTo(view.snp.top).offset(100)
            make.centerX.equalTo(view)
        })
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // CommonMethods.setStatusBarBackgroundColor(color: .black)
        test2()
        createTextField()
        // If user already added card
        self.paymentBtn.isHidden = true
        if self.userCCInfoArray != nil{
            if self.userCCInfoArray.count > 0{
                self.saveChangesBtn.isHidden = false
                self.deleteCardBtn.isHidden = false
                let dateData = self.userCCInfoArray[0].expiryDate?.components(separatedBy: "/")
                cardTest1?.paymentCardTextFieldDidChange(cardNumber: "\(self.userCCInfoArray[0].card_No!)", expirationYear: UInt(dateData![1])!, expirationMonth: UInt(dateData![0])!, cvc: "")
                let card: STPCardParams = STPCardParams()
                card.number = "\(self.userCCInfoArray[0].card_No!)"
                card.expMonth = UInt(dateData![0])!
                card.expYear = UInt(dateData![1])!
                paymentTextField.cardParams = card
            }
        }else{
            self.saveChangesBtn.isHidden = true
            self.deleteCardBtn.isHidden = true
            self.doneBtn.isHidden = false
            self.titleLabel.text = "Add Card"
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        guard let isComingFrmUnlock = CommonMethods.getValueFromUserDefaults(key: STRINGS.ISFROMUNLOCK)else{
            return
        }
        if isComingFrmUnlock == "true"{
            self.saveChangesBtn.isHidden = true
            self.deleteCardBtn.isHidden = true
            self.paymentBtn.isHidden = false
            self.doneBtn.isHidden = true
            self.titleLabel.text = "Payment"
        }
    }
    func createTextField() {
        paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.size.width - 30, height: 44)
        paymentTextField.delegate = self
        paymentTextField.translatesAutoresizingMaskIntoConstraints = false
        paymentTextField.borderWidth = 0
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: paymentTextField.frame.size.height - width, width:  paymentTextField.frame.size.width, height: paymentTextField.frame.size.height)
        border.borderWidth = width
        paymentTextField.layer.addSublayer(border)
        paymentTextField.layer.masksToBounds = true
        
        view.addSubview(paymentTextField)
        
        NSLayoutConstraint.activate([
            paymentTextField.topAnchor.constraint(equalTo: creditCardView.bottomAnchor, constant: 20),
            paymentTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            paymentTextField.widthAnchor.constraint(equalToConstant: self.view.frame.size.width-20),
            paymentTextField.heightAnchor.constraint(equalToConstant: 44)
            ])
    }

    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        cardTest1?.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: textField.expirationYear, expirationMonth: textField.expirationMonth, cvc: textField.cvc)
        self.expYear = String(format: "%d", textField.expirationYear)
        self.expMonth = String(format: "%d", textField.expirationMonth)
    }
    func paymentCardTextFieldDidEndEditing(_ textField: STPPaymentCardTextField) {
        //creditCardView.paymentCardTextFieldDidEndEditingExpiration(expirationYear: textField.expirationYear)
    }
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
       // creditCardView.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        //creditCardView.paymentCardTextFieldDidEndEditingCVC()
    }
    @IBAction func paymentBtnActn(_ sender : UIButton){
        
        if validateCard(){
            ProgressHUD.show("Payment processing..")
           completeYourPayment()
        }
    }
    @IBAction func deleteCardBtnActn(_ sender : UIButton){
         if validateCard(){
         callApiForDeleteCard()
        }
    }
    @IBAction func saveChangesBtnActn(_ sender : UIButton){
        if validateCard(){
        callApiUpdateCreditCard()
        }
    }
    @IBAction func doneBtnActn(_ sender : UIButton){
        if validateCard(){
            callApiAddCreditCard()
        }
    }
    func validateCard()-> Bool{
        self.view.endEditing(true)
        if STPCardValidator.validationState(forNumber: paymentTextField.cardNumber, validatingCardBrand: true) == .invalid || STPCardValidator.validationState(forNumber: paymentTextField.cardNumber, validatingCardBrand: true) == .incomplete || paymentTextField.cardNumber == nil {
            CommonMethods.getSnackBarWithMessage(message: ERRORS.INVALID_CARD_ERROR)
            return false
        }
        if self.expMonth != nil || self.expYear != nil{
            if STPCardValidator.validationState(forExpirationYear: self.expYear!, inMonth: self.expMonth!) == .invalid || STPCardValidator.validationState(forExpirationYear: self.expYear!, inMonth: self.expMonth!) == .incomplete{
                CommonMethods.getSnackBarWithMessage(message: ERRORS.INVALID_CARD_EXP_ERROR)
                return false
            }
            
        }else{
            CommonMethods.getSnackBarWithMessage(message: ERRORS.INVALID_CARD_EXP_ERROR)
            return false
        }
//        if (cardTest1?.cardholderLabel.text?.isEmpty)! || (cardTest1?.cardholderLabel.text?.isEmpty)!{
//            CommonMethods.getSnackBarWithMessage(message: ERRORS.INVALID_CARD_HOLDER_ERROR)
//            cardTest1?.cardholderLabel.becomeFirstResponder()
//            return false
//        }
        return true
    }
    
    @IBAction func backBtnActn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func completeYourPayment(){
        STPAPIClient.shared().createToken(withCard: paymentTextField.cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                return
            }
            
            self.dictPayData["stripe_token"] = token.tokenId
            print(self.dictPayData)
            self.callApiForPayment()
            
        }
    }
    
    func callApiAddCreditCard(){
        let cardNum =  Int64(paymentTextField.cardNumber!)
        let param: Parameters = ["card_No": cardNum!, "expiryDate": (cardTest1?.creditCardLastUsage.text)!]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.userManagemnet, showLoading: true) { (true, result) in

            if let isResult = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
                    if loginResp.success == "true" {
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                         CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    
    func callApiUpdateCreditCard(){
        let cardNum =  Int64(paymentTextField.cardNumber!)
        let param: Parameters = ["card_No": cardNum!, "expiryDate": (cardTest1?.creditCardLastUsage.text)!]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: self.userCCInfoArray[0].id!, requestType: RequestType.put, headers: headerParams, parameters: param, apiName: "", showLoading: true) { (true, result) in
            
            if let isResult = result{
                do {
//                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
//                    if loginResp.success == "true" {
//                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
//                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    func callApiForDeleteCard(){
        //let param : Parameters = ["id" : self.userCCInfoArray[0].id!]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: self.userCCInfoArray[0].id!, requestType: RequestType.delete, headers: headerParams, parameters: nil, apiName: "", showLoading: true) { (true, result) in
            
            if let isResult = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
                    if loginResp.success == "true" {
                        self.navigationController?.popViewController(animated: true)
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                    }else{
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
    
    
    func callApiForPayment(){
        
        let param: Parameters = ["amount": 110, "stripe_token": self.dictPayData.value(forKey: "stripe_token") as Any]
        let headerParams = ["Content-Type":"application/json", "Authorization": "Token \(CommonMethods.getToken())"]
        NetworkManager.callApi(baseUrl: AppUrl.BASE_URL, requestType: RequestType.post, headers: headerParams, parameters: param, apiName: AppApis.payment, showLoading: false) { (true, result) in
            
            if let isResult = result{
                do {
                    let loginResp = try JSONDecoder().decode(LoginResponse.self, from: isResult)
                    ProgressHUD.dismiss()
                    if loginResp.success == "True" {
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                        //paymentViewController.dismiss(animated: true, completion: {
                            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                UserDefaults.standard.set(true, forKey: STRINGS.IS_TRANSACTION_SUCCESS)
                                CommonMethods.saveValueInUserDefaults(Value: "false", forKey: STRINGS.ISFROMUNLOCK)
                                (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                            }
                       // })
                    }else{
                        CommonMethods.getSnackBarWithMessage(message: loginResp.message!)
                    }
                }catch (let error){
                    print(error)
                }
            }
        }
    }
}


