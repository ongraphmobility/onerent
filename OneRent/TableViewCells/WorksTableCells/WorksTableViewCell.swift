//
//  WorksTableViewCell.swift
//  OneRent
//
//  Created by Ongraph Technology on 16/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class WorksTableViewCell: UITableViewCell {
   
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
