//
//  WaiverTableCell.swift
//  OneRent
//
//  Created by Ambuj Singh on 10/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class WaiverTableCell: UITableViewCell {

    @IBOutlet weak var checkUncheckImage : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
