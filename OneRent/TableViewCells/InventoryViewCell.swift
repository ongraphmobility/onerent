//
//  InventoryViewCell.swift
//  OneRent
//
//  Created by Ongraph Technology on 19/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class InventoryViewCell: UITableViewCell {

    @IBOutlet weak var imagBoard : UIImageView!
    @IBOutlet weak var lblNumOfBoard : UILabel!
    @IBOutlet weak var lblText : UILabel!
    @IBOutlet weak var numBgView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.numBgView.layer.borderColor = UIColor.black.cgColor
        self.numBgView.layer.cornerRadius = (self.numBgView.frame.size.width)/2
        self.numBgView.layer.masksToBounds = true
//        self.numBgView.layer.borderWidth = 1.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDataOnBorad(data: PlatformData){
        self.imagBoard.sd_setImage(with: URL(string: AppUrl.BASE_URL + (data.image?.dropFirst() ?? "")), placeholderImage: UIImage(named: "glide_slup_iPhone"))
        self.lblNumOfBoard.text = String(describing: data.unused_boat! )
        self.lblText.text = "$\(String(describing: data.price_before_3_hours! ))/min & $\(String(describing: data.price_after_3_hours! ))/min after 3 hours \n\(data.board_Name!)"
    }
    /*func setData(data:GLIDE_SUP_BOARD ,withText: String){
       self.imagBoard.sd_setImage(with: URL(string: AppUrl.BASE_URL + (data.image?.dropFirst() ?? "")), placeholderImage: UIImage(named: "glide_slup_iPhone"))
        self.lblNumOfBoard.text = String(describing: data.unused_boat! )
        self.lblText.text = "$\(String(describing: data.price_before_3_hours! ))/min & $\(String(describing: data.price_after_3_hours! ))/min after 3 hours \n\(withText)"
    }
    func setData(data:PERCEPTION_HIGH_LIFE_HYBRID ,withText: String){
        
        self.imagBoard.sd_setImage(with: URL(string: AppUrl.BASE_URL + (data.image?.dropFirst() ?? "")), placeholderImage: UIImage(named: "perception-board_iPhone"))
        self.lblNumOfBoard.text = String(describing: data.unused_boat! )
        self.lblText.text = "$\(String(describing: data.price_before_3_hours! ))/min & $\(String(describing: data.price_after_3_hours! ))/min after 3 hours \n\(withText)"
    }
    func setData(data:LIFE_SUP_BOARD ,withText: String){
        
        self.imagBoard.sd_setImage(with: URL(string: AppUrl.BASE_URL + (data.image?.dropFirst() ?? "")), placeholderImage: UIImage(named: "sup_board_iPhone"))
        self.lblNumOfBoard.text = String(describing: data.unused_boat! )
        self.lblText.text = "$\(String(describing: data.price_before_3_hours! ))/min & $\(String(describing: data.price_after_3_hours! ))/min after 3 hours \n\(withText)"
    }
    func setData(data:PERCEPTION_REMBLER_KAYAK ,withText: String){
        
        self.imagBoard.sd_setImage(with: URL(string: AppUrl.BASE_URL + (data.image?.dropFirst() ?? "")), placeholderImage: UIImage(named: "perception_rembler_iPhone"))
        self.lblNumOfBoard.text = String(describing: data.unused_boat! )
        self.lblText.text = "$\(String(describing: data.price_before_3_hours! ))/min & $\(String(describing: data.price_after_3_hours! ))/min after 3 hours \n\(withText)"
    }*/
    
}
