//
//  ScannedPeriferalDeviceCell.swift
//  OneRent
//
//  Created by Ambuj Singh on 21/01/19.
//  Copyright © 2019 Ongraph Technology. All rights reserved.
//

import UIKit

class ScannedPeriferalDeviceCell: UITableViewCell {
    
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var wrapper : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        wrapper.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
