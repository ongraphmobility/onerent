//
//  HistoryTableViewCell.swift
//  OneRent
//
//  Created by Ambuj Singh on 26/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateTimeLabel : UILabel!
    @IBOutlet weak var rideTimeLabel : UILabel!
    @IBOutlet weak var rideAmountLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
