//
//  CustomMarkerView.swift
//  OneRent
//
//  Created by Ongraph Technology on 30/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import Foundation

class CustomMarkerView: UIView {
//    var img : UIImage!
    var borderColor : UIColor!
    
     init(frame: CGRect, borderColor: UIColor, tag: Int) {
        super.init(frame: frame)
//        self.img = image
        self.borderColor = borderColor
        self.tag = tag
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        let imgView = UIImageView(image: UIImage.init(named: "one_white_iPhone"))
        imgView.frame = CGRect(x: 0, y: 0, width: 100, height: 80 )
        imgView.contentMode = .scaleAspectFit
        self.addSubview(imgView)
    }
}
