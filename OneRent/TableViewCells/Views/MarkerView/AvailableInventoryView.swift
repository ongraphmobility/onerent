//
//  AvailableInventoryView.swift
//  OneRent
//
//  Created by Ongraph Technology on 30/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import Foundation

class AvailableInventoryView: UIView,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var inventoryTableView : UITableView!
    var platformDataArray = NSMutableArray()
    var customMarkerTag: Int!
    var dataArray = [PlatformData]()
    var prestentController : UIViewController!
    
    func loadInventoryTableView(){
        dataArray = (self.platformDataArray[customMarkerTag] as? [PlatformData])!
        inventoryTableView.estimatedRowHeight = 90
        inventoryTableView.rowHeight = UITableView.automaticDimension
        inventoryTableView.separatorStyle = .none
        inventoryTableView.delegate = self
        inventoryTableView.dataSource = self
        inventoryTableView.reloadData()
        
    }
   // func setData(data:PlatformData){
     
//        self.imgView1.downloaded(from: AppUrl.BASE_URL + (data.gLIDE_SUP_BOARD?.image?.dropFirst() ?? ""))
//        self.imgView2.downloaded(from: AppUrl.BASE_URL + (data.pERCEPTION_HIGH_LIFE_HYBRID?.image?.dropFirst() ?? ""))
//        self.imgView3.downloaded(from: AppUrl.BASE_URL + (data.lIFE_SUP_BOARD?.image?.dropFirst() ?? ""))
//        self.imgView4.downloaded(from: AppUrl.BASE_URL + (data.pERCEPTION_REMBLER_KAYAK?.image?.dropFirst() ?? ""))
//
//        self.lblBoard1.text = String(describing: data.gLIDE_SUP_BOARD?.unused_boat! ?? 0)
//        self.lblBoard2.text = String(describing: data.pERCEPTION_HIGH_LIFE_HYBRID?.unused_boat! ?? 0)
//        self.lblBoard3.text = String(describing: data.lIFE_SUP_BOARD?.unused_boat! ?? 0)
//        self.lblBoard4.text = String(describing: data.pERCEPTION_REMBLER_KAYAK?.unused_boat! ?? 0)
//
//        self.lblName1.text = "$\(String(describing: data.gLIDE_SUP_BOARD?.price_before_3_hours! ?? 0))/min & $\(String(describing: data.gLIDE_SUP_BOARD?.price_after_3_hours! ?? 0))/min after 3 hours \nGLIDE SUP BOARD"
//
//        self.lblName2.text = "$\(String(describing: data.pERCEPTION_HIGH_LIFE_HYBRID?.price_before_3_hours! ?? 0))/min & $\(String(describing: data.pERCEPTION_HIGH_LIFE_HYBRID?.price_after_3_hours! ?? 0))/min after 3 hours \nPERCEPTION HIGH-LIFE HYBRID"
//
//        self.lblName3.text = "$\(String(describing: data.lIFE_SUP_BOARD?.price_before_3_hours! ?? 0))/min & $\(String(describing: data.lIFE_SUP_BOARD?.price_after_3_hours! ?? 0))/min after 3 hours \nLIFE SUP BOARD"
//
//        self.lblName4.text = "$\(String(describing: data.pERCEPTION_REMBLER_KAYAK?.price_before_3_hours! ?? 0))/min & $\(String(describing: data.pERCEPTION_REMBLER_KAYAK?.price_after_3_hours! ?? 0))/min after 3 hours \nPERCEPTION REMBLER KAYAK"
        
   // }
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InventoryViewCell", for: indexPath) as! InventoryViewCell
        cell.selectionStyle = .none
        cell.setDataOnBorad(data: dataArray[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let BoardDetailViewControllerObj: BoardDetailViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.BOARDDETAILVC) as? BoardDetailViewController
        BoardDetailViewControllerObj?.titleStr = dataArray[indexPath.row].board_Name!
        BoardDetailViewControllerObj?.urlStr = dataArray[indexPath.row].board_details_file!
        prestentController.navigationController!.pushViewController(BoardDetailViewControllerObj!, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }

}
