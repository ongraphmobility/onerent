//
//  SideMenuView.swift
//  OneRent
//
//  Created by Ongraph Technology on 26/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

protocol SideMenuDelegate {
    
    func menuDelegate(_ bagroundView: UIView)
    
}

class SideMenuView: UIView,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var sideMenuNameLbl: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var prifileBagroundImgView: UIImageView!
    @IBOutlet weak var sideMenuTablView: UITableView!
    
    
    fileprivate var isSideMenuOpen : Bool = true
    fileprivate var sideMenuImagesArray = NSMutableArray()
    fileprivate var sideMenuSelectedStateImagesArray = NSMutableArray()
    fileprivate var sideMenuArray = NSMutableArray()
    var prestentController : UIViewController!
    
    var sideMenudelegate : SideMenuDelegate?
    
    
    
    func loadSideMenu(){
        
        //Set Side Menu Table view style
        sideMenuTablView.separatorStyle = .none
        sideMenuTablView.tableFooterView = UIView(frame: CGRect.zero)
        self.setDefaultValues()
        sideMenuTablView.delegate = self
        sideMenuTablView.dataSource = self
        sideMenuTablView.reloadData()
        
    }
    
    
    func setDefaultValues(){
       
        
        if let emailId = CommonMethods.getValueFromUserDefaults(key: STRINGS.USER_EMAIL) as? String{
            
            sideMenuNameLbl.text = emailId
        }
        sideMenuArray = DEFINED_ARRAYS.sideMenuNameArr
        isSideMenuOpen = false
        
        //Side menu selected state images  //blog_blue
      //  sideMenuSelectedStateImagesArray = ["menu_payments_iPhone","menu_rack_iPhone","menu_paddletips_iPhone","menu_history_iPhone","menu_safety_iPhone","menu_howitwork_iPhone","menu_contactus_iPhone","menu_settings_iPhone"]
         sideMenuSelectedStateImagesArray = ["menu_payments_iPhone","menu_boatmoney_iPhone","menu_infonew_iPhone","menu_history_iPhone","menu_safety_iPhone","menu_howitsworknew_iPhone","menu_contactus_iPhone","menu_settings_iPhone"]
        
    }
    
    
    // MARK: - ListTableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
       return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sideMenuArray.count
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 50
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell", for: indexPath) as! SideMenuTableCell
        
        cell.lblMenuName.text = sideMenuArray.object(at: indexPath.row) as? String
        CommonMethods.getLabelFontSizesWithDevices(label: cell.lblMenuName)
        cell.imgView.image = UIImage.init(named: sideMenuSelectedStateImagesArray.object(at: indexPath.row) as! String)
        cell.selectionStyle = .none
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == sideMenuTablView {
            switch indexPath.row {
            case 0:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let paymentViewControllerObj: PaymentsViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.PAYMENTS) as? PaymentsViewController
                CommonMethods.saveValueInUserDefaults(Value: "false", forKey: STRINGS.ISPAYMENTSKIP)
                CommonMethods.saveValueInUserDefaults(Value: "false", forKey: STRINGS.ISFROMUNLOCK)
                prestentController.navigationController?.pushViewController(paymentViewControllerObj!, animated: true)
            case 1:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let hostRackViewControllerObj: HostRackViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.HOSTRACKVC) as? HostRackViewController
                prestentController.navigationController?.pushViewController(hostRackViewControllerObj!, animated: true)
            case 2:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let paddleTipsViewControllerObj: PaddleTipsViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.PADDLETIPSVC) as? PaddleTipsViewController
                prestentController.navigationController?.pushViewController(paddleTipsViewControllerObj!, animated: true)
            case 3:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let rideHistoryViewControllerObj: RideHistoryViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.RIDEHISTORYVC) as? RideHistoryViewController
                prestentController.navigationController?.pushViewController(rideHistoryViewControllerObj!, animated: true)
            case 4:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let safetyTrainingViewControllerObj: SafetyTrainingViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.SAFETYVC) as? SafetyTrainingViewController
                prestentController.navigationController?.pushViewController(safetyTrainingViewControllerObj!, animated: true)
            case 5:
               let storyBoard = UIStoryboard(name: "Main", bundle: nil)
               let howWorksViewControllerObj: HowWorksViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.WORKSVC) as? HowWorksViewController
               howWorksViewControllerObj?.isfromSplash = false
               prestentController.navigationController?.pushViewController(howWorksViewControllerObj!, animated: true)
            case 6:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let ContactUsViewControllerObj: ContactUsViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.CONTACTUSVC) as? ContactUsViewController
                prestentController.navigationController?.pushViewController(ContactUsViewControllerObj!, animated: true)
            case 7:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let profileViewControllerObj: ProfileViewController? = storyBoard.instantiateViewController(withIdentifier: CONTROLLERS.PROFILE) as? ProfileViewController
                prestentController.navigationController?.pushViewController(profileViewControllerObj!, animated: true)
            default:
                break;
                //Default
            }
            isSideMenuOpen = false
            self.removeFromSuperview()
            sideMenudelegate?.menuDelegate(self)
            
        }
        
        
    }
    
    

    
    
   
    
    
   
    
  
    
}
