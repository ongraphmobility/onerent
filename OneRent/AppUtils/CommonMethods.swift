//
//  CommonMethods.swift
//  OneRent
//
//  Created by Ongraph Technology on 25/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class CommonMethods: NSObject {
    
    
    /*
     *  Method to validate Email.
     *
     *  @param Email in string
     */
    class func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /*
     *  Method to validate phone number .
     *
     *  @param Charters in string
     *  @return true/false
     */
    class func validatePhoneNumber(value: String) -> Bool {
        
        if value.count > 15 || value.count < 7 {
            return false
        }
        return true
    }
    
  
    
    /*
     *  Method to show alert with title.
     *
     *  @param key to show text in alert.
     */
    
    class func showAlert(withTitle alertTitle: String, alertMessage: String, sender: Any) {
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString(STRINGS.OK, comment: "OK action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        alertController.addAction(okAction)
        (sender as AnyObject).present(alertController, animated: true, completion: {})
    }
    
    /*
     *  Method to show alert with title and Ok Action.
     *
     *  @param key to show text in alert, message and Callback function
     */
    class func showAlert(withOk title: String, andMessage message: String, andCallMethodOnOk `func`: Selector, fromObject object: Any, on viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString(STRINGS.OK, comment: "OK action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //clang diagnostic push
            //clang diagnostic ignored "-Warc-performSelector-leaks"
            (object as AnyObject).perform(`func`)
            //clang diagnostic pop
            //   [object performSelector:func];
        })
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: {})
    }
    
    /*
     *  Method to Fetch device id.
     *
     *  @param time in string
     */
    class func getDeviceID() -> String{
        return UIDevice.current.identifierForVendor!.uuidString
        
    }
    
    /*
     *  Method to fetch token.
     */
    
    class func getToken() -> String{
        return self.getValueFromUserDefaults(key: STRINGS.TOKEN_KEY) ?? ""
    }
    
    class func getValueFromUserDefaults(key:String) -> String?{
        
        let value = UserDefaults.standard.object(forKey: key) as? String
        return value ?? nil
        
    }
    
    class func saveValueInUserDefaults(Value: String, forKey key: String) {
        
        UserDefaults.standard.set(Value, forKey: key)
        UserDefaults.standard.synchronize()
        
    }
    //MARK: - SnackBar Color and Message
    
    class func getSnackBarWithMessage(message: String){
        
        let snackbar = TTGSnackbar(message: message, duration: .middle)
        snackbar.show()
        snackbar.backgroundColor = UIColor.init(red: 0.0/255.0, green: 43.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        snackbar.leftMargin = 0
        snackbar.rightMargin = 0
        snackbar.bottomMargin = 0
        snackbar.layer.cornerRadius = 0
        
    }
    /*
     *  Method to get Header.
     */
    class func convertDataIntoString(data: Data) throws -> String
    {
        return String(decoding: data, as: UTF8.self)
    }
    /*
     *  Method to get Header.
     */
   class func header() -> HTTPHeaders
    {
        print(CommonMethods.getDeviceID())
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": CommonMethods.getDeviceID()
        ]
        return headers
    }
    /*
     *  Method to get Header.
     */
    class func headerWithToken() -> HTTPHeaders
    {
        print(CommonMethods.getDeviceID())
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": CommonMethods.getToken(),
            "Token" : CommonMethods.getToken()
        ]
        return headers
    }
    
    //MARK: Convert String to Dictionary
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    /*
     *  Method to make login VC as Root VC.
     */
    class func moveToLogin(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let appWindow = UIApplication.shared.delegate?.window!
        let storeViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navigatioStack = UINavigationController(rootViewController: storeViewController)
        navigatioStack.navigationBar.isHidden = true
        appWindow!.rootViewController = navigatioStack
        appWindow!.makeKeyAndVisible()
    }
    
    //
    // Convert String to base64
    //
    class func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //
    // Convert base64 to String
    //
    class func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    /*
     *  Method tor convert dictionary to string.
     */
    class func getJsonText(dictinary: NSDictionary) ->String{
        
        do {
            let jsonData = try? JSONSerialization.data(withJSONObject: dictinary, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            return NSString(data: jsonData!,
                            encoding: String.Encoding.ascii.rawValue) as String!
        }
    }
    
    /*
     *  Method to show alert with title, Cancel action and Ok action.
     *
     *  @param key to show text in alert, message and Callback function
     */
    class func showAlert(_ title: String, andMessage message: String, andCallMethodOnOk `func`: Selector, andCallMethodOnCancel fun: Selector, fromObject object: Any, on viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString(STRINGS.CANCEL, comment: "Cancel action"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            // perform selector is fun is not nil
            alertController.dismiss(animated: true, completion: nil)
        })
        
        //  Converted with Swiftify v1.0.6381 - https://objectivec2swift.com/
        let okAction = UIAlertAction(title: NSLocalizedString(STRINGS.OK, comment: "OK action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // perform selector is func is not nil
            if `func` != nil {
                //clang diagnostic push
                //clang diagnostic ignored "-Warc-performSelector-leaks"
                (object as AnyObject).perform(`func`)
                //clang diagnostic pop
            }
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: {})
    }
    //
    // Get Status Bar Color for Different Views
    //
   class func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    //
    // Get Font Size According to Different Devices
    //
    class func getLabelFontSizesWithDevices(label: UILabel) {
        switch UIDevice.current.deviceType {
            
        case .iPhone4_4S:
            label.font = UIFont(name: label.font.fontName, size: 10)
            
        case .iPhones_5_5s_5c_SE:
            label.font = UIFont(name: label.font.fontName, size: 13)
            
        case .iPhones_6_6s_7_8:
            label.font = UIFont(name: label.font.fontName, size: 15)
            
        case .iPhones_6Plus_6sPlus_7Plus_8Plus:
            label.font = UIFont(name: label.font.fontName, size: 17)
            
        case .iPhoneX:
            label.font = UIFont(name: label.font.fontName, size: 15)
            
        default:
            print("iPad or Unkown device")
            label.font = UIFont(name: label.font.fontName, size: 17)
            
        }
        //return label
    }
    
    /*
     *  Method to Convert time from HH:mm:ss to h:mm a.
     *
     *  @param time in string
     */
    class func convertTimeInAmPm(timeIs: String) -> String{
        let inFormatter = DateFormatter()
        inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        inFormatter.dateFormat = "HH:mm:ss"
        
        
        let outFormatter = DateFormatter()
        outFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        outFormatter.dateFormat = "hh:mm a"
        
        let date = inFormatter.date(from: timeIs)
        let outStr = outFormatter.string(from: date!)
        
        return outStr
        
        
    }
    
    
    class func convertTimeInHrs(timeIs: String) -> String{
        let inFormatter = DateFormatter()
        inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        inFormatter.dateFormat = "HH:mm:ss"
        
        
        let outFormatter = DateFormatter()
        outFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        outFormatter.dateFormat = "HH:mm"
        
        let date = inFormatter.date(from: timeIs)
        let outStr = outFormatter.string(from: date!)
        
        return outStr
        
        
    }
   class func dateDifferenceformatter(date: Date) -> DateComponents {
        
        let date1:Date = Date() // Same you did before with timeNow variable
        let date2: Date = date
        
        let calender:Calendar = Calendar.current
        var components: DateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date2, to: date1)
        print(components)
        print(components.second)
        let hours = components.hour! * 3600
        let minuts = components.minute! * 60
    
        let totseconds = hours+minuts
        components.second = totseconds
        return components
    }
   /* class func getCountryPhonceCode (_ country : String) -> String
    {
        var countryDictionary  = ["AF":"+93",
                                  "AL":"+355",
                                  "DZ":"+213",
                                  "AS":"+1",
                                  "AD":"+376",
                                  "AO":"+244",
                                  "AI":"+1",
                                  "AG":"+1",
                                  "AR":"+54",
                                  "AM":"+374",
                                  "AW":"+297",
                                  "AU":"+61",
                                  "AT":"+43",
                                  "AZ":"+994",
                                  "BS":"+1",
                                  "BH":"+973",
                                  "BD":"+880",
                                  "BB":"+1",
                                  "BY":"+375",
                                  "BE":"+32",
                                  "BZ":"+501",
                                  "BJ":"+229",
                                  "BM":"+1",
                                  "BT":"+975",
                                  "BA":"+387",
                                  "BW":"+267",
                                  "BR":"+55",
                                  "IO":"+246",
                                  "BG":"+359",
                                  "BF":"+226",
                                  "BI":"+257",
                                  "KH":"+855",
                                  "CM":"+237",
                                  "CA":"+1",
                                  "CV":"+238",
                                  "KY":"+345",
                                  "CF":"+236",
                                  "TD":"+235",
                                  "CL":"+56",
                                  "CN":"+86",
                                  "CX":"+61",
                                  "CO":"+57",
                                  "KM":"+269",
                                  "CG":"+242",
                                  "CK":"+682",
                                  "CR":"+506",
                                  "HR":"+385",
                                  "CU":"+53",
                                  "CY":"+537",
                                  "CZ":"+420",
                                  "DK":"+45",
                                  "DJ":"+253",
                                  "DM":"+1",
                                  "DO":"+1",
                                  "EC":"+593",
                                  "EG":"+20",
                                  "SV":"+503",
                                  "GQ":"+240",
                                  "ER":"+291",
                                  "EE":"+372",
                                  "ET":"+251",
                                  "FO":"+298",
                                  "FJ":"+679",
                                  "FI":"+358",
                                  "FR":"+33",
                                  "GF":"+594",
                                  "PF":"+689",
                                  "GA":"+241",
                                  "GM":"+220",
                                  "GE":"+995",
                                  "DE":"+49",
                                  "GH":"+233",
                                  "GI":"+350",
                                  "GR":"+30",
                                  "GL":"+299",
                                  "GD":"+1",
                                  "GP":"+590",
                                  "GU":"+1",
                                  "GT":"+502",
                                  "GN":"+224",
                                  "GW":"+245",
                                  "GY":"+595",
                                  "HT":"+509",
                                  "HN":"+504",
                                  "HU":"+36",
                                  "IS":"+354",
                                  "IN":"+91",
                                  "ID":"+62",
                                  "IQ":"+964",
                                  "IE":"+353",
                                  "IL":"+972",
                                  "IT":"+39",
                                  "JM":"+1",
                                  "JP":"+81",
                                  "JO":"+962",
                                  "KZ":"+77",
                                  "KE":"+254",
                                  "KI":"+686",
                                  "KW":"+965",
                                  "KG":"+996",
                                  "LV":"+371",
                                  "LB":"+961",
                                  "LS":"+266",
                                  "LR":"+231",
                                  "LI":"+423",
                                  "LT":"+370",
                                  "LU":"+352",
                                  "MG":"+261",
                                  "MW":"+265",
                                  "MY":"+60",
                                  "MV":"+960",
                                  "ML":"+223",
                                  "MT":"+356",
                                  "MH":"+692",
                                  "MQ":"+596",
                                  "MR":"+222",
                                  "MU":"+230",
                                  "YT":"+262",
                                  "MX":"+52",
                                  "MC":"+377",
                                  "MN":"+976",
                                  "ME":"+382",
                                  "MS":"+1",
                                  "MA":"+212",
                                  "MM":"+95",
                                  "NA":"+264",
                                  "NR":"+674",
                                  "NP":"+977",
                                  "NL":"+31",
                                  "AN":"+599",
                                  "NC":"+687",
                                  "NZ":"+64",
                                  "NI":"+505",
                                  "NE":"+227",
                                  "NG":"+234",
                                  "NU":"+683",
                                  "NF":"+672",
                                  "MP":"+1",
                                  "NO":"+47",
                                  "OM":"+968",
                                  "PK":"+92",
                                  "PW":"+680",
                                  "PA":"+507",
                                  "PG":"+675",
                                  "PY":"+595",
                                  "PE":"+51",
                                  "PH":"+63",
                                  "PL":"+48",
                                  "PT":"+351",
                                  "PR":"+1",
                                  "QA":"+974",
                                  "RO":"+40",
                                  "RW":"+250",
                                  "WS":"+685",
                                  "SM":"+378",
                                  "SA":"+966",
                                  "SN":"+221",
                                  "RS":"+381",
                                  "SC":"+248",
                                  "SL":"+232",
                                  "SG":"+65",
                                  "SK":"+421",
                                  "SI":"+386",
                                  "SB":"+677",
                                  "ZA":"+27",
                                  "GS":"+500",
                                  "ES":"+34",
                                  "LK":"+94",
                                  "SD":"+249",
                                  "SR":"+597",
                                  "SZ":"+268",
                                  "SE":"+46",
                                  "CH":"+41",
                                  "TJ":"+992",
                                  "TH":"+66",
                                  "TG":"+228",
                                  "TK":"+690",
                                  "TO":"+676",
                                  "TT":"+1",
                                  "TN":"+216",
                                  "TR":"+90",
                                  "TM":"+993",
                                  "TC":"+1",
                                  "TV":"+688",
                                  "UG":"+256",
                                  "UA":"+380",
                                  "AE":"+971",
                                  "GB":"+44",
                                  "US":"+1",
                                  "UY":"+598",
                                  "UZ":"+998",
                                  "VU":"+678",
                                  "WF":"+681",
                                  "YE":"+967",
                                  "ZM":"+260",
                                  "ZW":"+263",
                                  "BO":"+591",
                                  "BN":"+673",
                                  "CC":"+61",
                                  "CD":"+243",
                                  "CI":"+225",
                                  "FK":"+500",
                                  "GG":"+44",
                                  "VA":"+379",
                                  "HK":"+852",
                                  "IR":"+98",
                                  "IM":"+44",
                                  "JE":"+44",
                                  "KP":"+850",
                                  "KR":"+82",
                                  "LA":"+856",
                                  "LY":"+218",
                                  "MO":"+853",
                                  "MK":"+389",
                                  "FM":"+691",
                                  "MD":"+373",
                                  "MZ":"+258",
                                  "PS":"+970",
                                  "PN":"+872",
                                  "RE":"+262",
                                  "RU":"+7",
                                  "BL":"+590",
                                  "SH":"+290",
                                  "KN":"+1",
                                  "LC":"+1",
                                  "MF":"+590",
                                  "PM":"+508",
                                  "VC":"+1",
                                  "ST":"+239",
                                  "SO":"+252",
                                  "SJ":"+47",
                                  "SY":"+963",
                                  "TW":"+886",
                                  "TZ":"+255",
                                  "TL":"+670",
                                  "VE":"+58",
                                  "VN":"+84",
                                  "VG":"+284",
                                  "VI":"+340"]
        if countryDictionary[country] != nil {
            return countryDictionary[country]!
        }else{
            return ""
        }
        
    }*/
}
extension UIDevice {
    
    
    enum DeviceType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown = "iPadOrUnknown"
    }
    
    var deviceType: DeviceType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
    
}

open class UIInsetLabel: UILabel {
    
    open var insets : UIEdgeInsets = UIEdgeInsets() {
        didSet {
            super.invalidateIntrinsicContentSize()
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.width += insets.left + insets.right
        size.height += insets.top + insets.bottom
        return size
    }
    
    override open func drawText(in rect: CGRect) {
        return super.drawText(in: rect.inset(by: insets))
    }
}
