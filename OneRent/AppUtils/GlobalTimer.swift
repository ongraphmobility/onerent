//
//  GlobalTimer.swift
//  OneRent
//
//  Created by Ambuj Singh on 25/12/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class GlobalTimer: NSObject {

    class func getInstance() -> GlobalTimer {
        struct Static {
            static let instance : GlobalTimer = GlobalTimer()
        }
        return Static.instance
    }
    
    var seconds = 00
    var isTimerRunning = false
    weak var timer: Timer?
    var totalTime : String? = "0"
    
    
    func startTimer() {
        if isTimerRunning == false {
            runTimer()
        }
    }
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    func stopTimer(){
        timer?.invalidate()
        totalTime = timeString(time: TimeInterval(seconds))
        print(seconds)
        isTimerRunning = false
    }
    @objc func updateTimer() {
        seconds += 1
        print(seconds)
        totalTime = timeString(time: TimeInterval(seconds))
    }
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
}
