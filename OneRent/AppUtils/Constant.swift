//
//  Constant.swift
//  OneRent
//
//  Created by Ongraph Technology on 25/10/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import Foundation


struct STRINGS {
    
    static let ALERT = "Alert"
    static let CANCEL = "Cancel"
    static let OK = "Ok"
    
    static let TOKEN_KEY = "TokenKey"
    static let TOKEN_VALUE = "tokenIs"
    static let LOADING = "Loading..."
    static let ISPAYMENTSKIP = "IsPaymentSkip"
    static let ISFROMUNLOCK = "IsFromUnlock"
    
    static let GUEST_USER = "Guest"
    static let LOGGEDIN_USER = "User"
    static let USER_TYP_KEY = "UserTyp"
    static let IS_USER_VERIFIED = "IsUserVerified"
    static let USER_EMAIL = "UserEmail"
    static let USER_FULLNAME = "UserName"
    static let USER_PHONENUMBER = "UserPhoneNumber"
    static let USER_PROFILE_PICTURE = "UserProfilePicture"
    static let USER_PROFILE_IMAGE = "ProfileImgPicked"
    static let USER_ID = "UserId"
    static let USER_DYNAMIC_DATA = "UserDynamic"
    static let IS_BOARD_AACTIVE = "IsBoardActive"
    static let IS_TRANSACTION_SUCCESS = "IsTransactionSuccess"
    static let IS_REPORT_SUBMITTED = "IsReportSubmitted"
    static let QRSCAN_MODEL_DATA = "QRScanModel"
    static let LINKA_LOCK_ID = "LinkaLockId"
    static let TIME_BEFORE = "Before_time"
    static let TIME_AFTER = "After_time"
    static let TIME_RIDETIME = "Ride_time"
    static let TEXT_RIDETIME = "Ride_timeText"

}



struct MESSAGES {
    static let MAIL_SEND = "Please check your email for the verification code"
    static let LOGOUT_MSG = "Are you sure you want to logout?"
}




struct ERRORS {
    
    static let INVALID_DOB = "Please enter valid DOB"
    static let ERROR = "Error"
    static let FAILURE = "Failure"
    static let INTERNET_ERROR = "No Internet Connection"
    static let INVALID_EMAIL_ERROR = "Please Enter a valid Email"
    static let INVALID_ZIPCODE_ERROR = "Please Enter Zip Code"
    static let INVALID_CARD_ERROR = "Please Enter a valid Card Number"
    static let INVALID_CARD_EXP_ERROR = "Please Enter a valid Expiery Date"
    static let INVALID_CARD_HOLDER_ERROR = "Please Enter a valid Card Holder name"
    static let INVALID_CODE = "Please enter valid Code"
    static let INVALID_LOGIN_CODE = "Login Code is invalid"
    static let INVALID_LOGIN = "Something went wrong"
    static let EMPTY_FIELD = "Please enter a"
    static let NO_BOARD = "No board is available"
}

struct CONTROLLERS{
    static let PROFILE = "ProfileViewController"
    static let PAYMENTS = "PaymentsViewController" 
    static let ADDCARD = "AddCardViewController" 
    static let NUMERICCODEVC = "NumericViewController" 
    static let SCANQRVC = "ScanQRViewController" 
    static let BOARDDETAILVC = "BoardDetailViewController"
    static let SPLASHVC = "SplashViewController"
    static let WORKSVC = "HowWorksViewController"
    static let WAIVERVC = "WaiverViewController"
    static let WAIVEREMAILVC = "WaiverEmailedViewController"
    static let RIDEHISTORYVC = "RideHistoryViewController" 
    static let HOSTRACKVC = "HostRackViewController"
    static let PADDLETIPSVC = "PaddleTipsViewController"
    static let CONTACTUSVC = "ContactUsViewController" 
    static let SAFETYVC = "SafetyTrainingViewController"
}

struct AppUrl {
//    static let BASE_URL = "http://demo2.ongraph.com:5000/"
    static let BASE_URL = "http://34.196.255.185/"
}
struct AppUrlKey {
//    static let StripeLiveKey = "key_live_bkVgpunxukGwUIQss9u6QkfpusdP3if1"
    static let StripeLiveKey = "pk_live_KLOPWxuU467w21NUtsMFr2wp"
//    static let StripeTestKey = "pk_test_gG9vMys4soGoOstRaZPsUjd7"
    static let StripeTestKey = "pk_test_utTXSHmZb7SldLcwpagckqBT" // from new account
    static let termsOfService = "http://oneboardrentals.com/terms-of-service/"
    static let privacyPolicy = "http://oneboardrentals.com/privacy-policy/"
    static let aggreementPolicy = "http://oneboardrentals.com/rental-agreement/"
}
struct AppApis {
    static let dynamic                  = "dynamic/"
    static let login                    = "login/"
    static let code                     = "code/"
    static let platformList             = "platformlist/"
    static let logout                   = "auth/logout/"
    static let userProflie              = "auth/user/"
    static let verfifyEmail             = "email/verify/"
    static let userManagemnet           = "CC/"
    static let payment                  = "payment/"
    static let agreement                = "agreement/"
    static let qrScan                   = "qrscan/"
    static let unlock                   = "unlock/"
    static let lock                     = "lock/"
    static let report                   = "report/" 
    static let damageReport             = "damageboardreport/"
    static let damageImage              = "damageimage/"
    static let feedback                 = "feedback/"
    static let history                  = "history/"
    static let paddleTips               = "websitelink/"
    static let hostRack                 = "hostrack/"
    static let appCancel                = "aftercancelingapp/"
    static let contactUs                = "contactus/"
    
}
struct DEFINED_ARRAYS {

    static let sideMenuNameArr : NSMutableArray = ["P A Y M E N T S","H O S T  A  R A C K","P A D D L E  T I P S","R I D E  H I S T O R Y","S A F E T Y  &  T R A I N I N G","H O W  I T  W O R K S","C O N T A C T  U S","S E T T I N G S"]
    static let paymentMethodArray : NSMutableArray = ["ENTER CREDIT CARD","SNAP CREDIT CARD"]
    static let paymentMethodImageArray : NSMutableArray = ["card_icon_iPhone","camera_line_iPhone"]
    
    static let howWorksTitleArr : NSMutableArray = ["1 . F I N D  &  C H O O S E","2 . S A F E T Y  &  W A I V E R","3 . S C A N  &  U N L O C K","4 . E N D  R I D E"]
    static let howWorksDescpArr : NSMutableArray = ["Find a rack near you and check availability of paddle boards and kayaks.", "Read all of the safety instructions and local guidelines carefully and sign the liability waiver.","Choose the paddle board or kayak you wish to ride and scan the QR Code and One Rack.","Click End Ride and lock the board or kayak and equipment back into original location on the rack."]
    static let howWorksimgArr : NSMutableArray = ["map_pin_iPhone","jackets_iPhone","direction_icon_iPhone","mobile_icon_iPhone"]
    static var waiverTitleArr : NSMutableArray = [["isAdded":false, "title":"Life Jacket is required"],["isAdded":false, "title":"One rider per Paddleboard"],["isAdded":false, "title":"Obey all laws"],["isAdded":false, "title":"Ride at your own risk"],["isAdded":false, "title":"I am 18 year old or older"]]
}

struct TIME {
    static var before_time : Int?
    static var after_time : Int?
    static var totalRideTime : Int?
}

