//
//  DynamicUserDataUtility.swift
//  OneRent
//
//  Created by Ambuj Singh on 30/11/18.
//  Copyright © 2018 Ongraph Technology. All rights reserved.
//

import UIKit

class DynamicUserDataUtility: NSObject {

    class func getInstance() -> DynamicUserDataUtility {
        struct Static {
            static let instance : DynamicUserDataUtility = DynamicUserDataUtility()
        }
        return Static.instance
    }
    func setValues(data: [DynamicModel]){
        id = data[0].id
        zipcode_required_field = data[0].zipcode_required_field
        pricing = data[0].pricing
        agreement_age_text = data[0].agreement_age_text
        bad_weather = data[0].bad_weather
        agreement_file = data[0].agreement_file
        extra_min_text = data[0].extra_min_text
        extra_min = data[0].extra_min
        main_logo_screen_tagline = data[0].main_logo_screen_tagline
        screen_after_lock = data[0].screen_after_lock
    }
    var id : Int? = 0
    var zipcode_required_field : Bool? = false
    var pricing : String? = ""
    var agreement_age_text : String? = ""
    var bad_weather : Bool? = false
    var agreement_file : String? = ""
    var extra_min_text : String? = ""
    var extra_min : Int? = 0
    var main_logo_screen_tagline : String? = ""
    var screen_after_lock : String? = ""
    
    
}
