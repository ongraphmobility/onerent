//
//  LinkaAPIManager.swift
//  LockTestAPIiOS
//
//  Created by Leung Yu Wing on 8/10/2016.
//  Copyright © 2016年 Vanport. All rights reserved.
//

import UIKit
import CoreLocation

class AppLinkaAPIManager: NSObject, LinkaAPIProtocol, CLLocationManagerDelegate {

    
    // Please change the API Key & Secret key to corresponding merchant account
  
    static var LinkaMerchantAPIKey = "c6c360c1-1b71-4948-9bdc-f358e2692220"
    static var LinkaMerchantSecretKey = "4482cf06-dbcf-4f4e-81bf-4610be826d7b"

    // IMPORTANT
    // Set this value to TRUE if the user must double press the power button to lock the device. If this value is TRUE, locking through app is disabled.
    // Set this value to FALSE if the locking command is sent through the app. If this value is FALSE, the device will not lock if any motion is detected.
    static var isButtonUsedForLocking = false
    
    var locationManager : CLLocationManager!
    
    
    func Linka_locationManager() -> CLLocationManager!
    {
        return locationManager
    }
    func LinkaMerchantAPI_getAPIKey() -> String!
    {
        return AppLinkaAPIManager.LinkaMerchantAPIKey
    }
    func LinkaMerchantAPI_getSecretKey() -> String!
    {
        return AppLinkaAPIManager.LinkaMerchantSecretKey
    }
    
    func LinkaMerchantAPI_getIsButtonUsed() -> Bool
    {
        return AppLinkaAPIManager.isButtonUsedForLocking
    }
    
    func initializeLocationManager()
    {
        locationManager = LinkaMerchantAPIService.makeLocationManager()
        locationManager.delegate = self
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    var curLocation : CLLocation!
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        curLocation = locations[0];
    }
    
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {

    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
    }
}
