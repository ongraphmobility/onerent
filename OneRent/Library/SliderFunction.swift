//
//  SliderFunction.swift
//  MyFlightSearchMFS
//
//  Created by Sagar on 23/05/17.
//  Copyright © 2017 MFS. All rights reserved.
//

import UIKit

class SliderFunction: NSObject {
    
    class func slideView(uiv_slide: UIView, withDuration d_duration: Double, toX xValue: CGFloat, andY yValue: CGFloat, onCompletion moveView: UIView, toX valueX: CGFloat, andY valueY: CGFloat, button Sender: UIButton) {
        
        UIView.animate(withDuration: d_duration, animations: {() -> Void in
            uiv_slide.frame = CGRect(x: xValue, y: yValue, width: uiv_slide.frame.size.width, height: uiv_slide.frame.size.height)
            
            
            Sender.isEnabled = false
        }, completion: {(finished: Bool) -> Void in
            moveView.frame = CGRect(x: valueX,y: valueY, width: moveView.frame.size.width, height: moveView.frame.size.height)
            Sender.isEnabled = true
        })
        
        
    }
    
    class func imageView(uiv_slide: UIView, withDuration d_duration: Double, toX xValue: CGFloat, andY yValue: CGFloat) {
        //Make an animation to slide the view off the screen
        UIView.animate(withDuration: d_duration, animations: {() -> Void in
            uiv_slide.frame = CGRect(x: xValue, y: yValue, width: uiv_slide.frame.size.width, height: uiv_slide.frame.size.height)
        }, completion: {(finished: Bool) -> Void in
        })
    }
}



